/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.file;

import metro.data.Station;

/**
 *
 * @author Dan
 */
public class MarkedStation {
    private final Station station;
    private final long id;
    
    public MarkedStation(Station station, long id){
        this.station = station;
        this.id = id;
    }

    public Station getStation() {
        return station;
    }

    public long getId() {
        return id;
    }
    
}
