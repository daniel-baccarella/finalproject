package metro.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.scene.paint.Color;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javax.imageio.ImageIO;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import metro.data.LineNode.LineNodeBuilder;
import metro.data.MetroLine;
import metro.data.Station;
import metro.data.metroData;
import metro.gui.metroWorkspace;
import metro.metroApp;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class metroFiles implements AppFileComponent {
    // FOR JSON LOADING
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_STATIONS = "stations";
    static final String JSON_LINES = "metro_lines";
    static final String JSON_SHAPE = "shape";
    static final String JSON_TYPE = "type";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_RADIUS = "radius";
    static final String JSON_FILL_COLOR = "fill_color";
    static final String JSON_OUTLINE_COLOR = "outline_color";
    static final String JSON_OUTLINE_THICKNESS = "outline_thickness";
    
    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALUE = "";
    
    //TYPES
    static final String STATION_TYPE = "metro_station";
    static final String LINE_TYPE = "metro_line";
    
    static final String JSON_ID = "id";
    static final String JSON_NAME = "name";
    static final String JSON_STATION_ARRAY = "station_array";
    
    static final String JSON_START_LINE_NODE_X = "start_line_x";
    static final String JSON_START_LINE_NODE_Y = "start_line_y";
    static final String JSON_END_LINE_NODE_X = "end_line_x";
    static final String JSON_END_LINE_NODE_Y = "end_line_y";
    
    //FOR NEW FEATURES
    static final String JSON_CANVAS_WIDTH = "canvas_width";
    static final String JSON_CANVAS_HEIGHT = "canvas_height";
 
    private final metroApp app;
    
    static final String JSON_TEXT_STRING = "text_string";
    static final String JSON_FONT_FAMILY = "font_family";
    static final String JSON_BOLD_ATTRIBUTE = "bold";
    static final String JSON_ITALIC_ATTRIBUTE = "italic";
    static final String JSON_FONT_SIZE = "font_size";
    static final String JSON_FONT_ORIENTATION = "font_orientation";
    static final String JSON_FONT_COLOR = "font_color";
    
    static final String JSON_FILE_PATH = "file_path";
    
    //  WEB JSON VARS
    static final String WEB_NAME = "name";
    static final String WEB_LINES = "lines";
    static final String WEB_CIRCULAR = "circular";
    static final String WEB_COLOR = "color";
    static final String WEB_STATION_NAMES = "station_names";
    static final String WEB_STATIONS = "stations";
    static final String WEB_X = "x";
    static final String WEB_Y = "y";
    
    public metroFiles(metroApp app){
        this.app = app;
    }
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that together draws the logo.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	metroData dataManager = (metroData)data;
        HashMap<Station, Long> stationIDs = new HashMap<>();
        
        //Put the stations into a set with unique id's
        Set<Station> stations  = dataManager.getStations();
        long id = 0;
        for(Station station: stations){
            stationIDs.put(station, id++);
        }
        
        // FIRST THE BACKGROUND COLOR
	Color bgColor = dataManager.getBackgroundColor();
	JsonObject bgColorJson = makeJsonColorObject(bgColor);
        
        //Make JSON ARRAY FOR STATIONS
        JsonArrayBuilder stationsArrayBuilder = Json.createArrayBuilder();
        for(Station station: stations){
            JsonObject stationJSON = makeJsonStationObject(station, stationIDs.get(station));
            stationsArrayBuilder.add(stationJSON);
        }
        JsonArray stationsJSONArray = stationsArrayBuilder.build();
        
        //MAKE JSON ARRAY FOR LINES
        Set<MetroLine> linesSet = dataManager.getMetroLines();
        JsonArrayBuilder lineArrayBuilder = Json.createArrayBuilder();
        for(MetroLine line: linesSet){
            JsonObject stationJSON = makeJsonLineObject(line, stationIDs);
            stationsArrayBuilder.add(stationJSON);
        }
        JsonArray linesJSONArray = stationsArrayBuilder.build();
        
        // THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_BG_COLOR, bgColorJson)
                .add(JSON_CANVAS_WIDTH, dataManager.getCanvas().getWidth())
                .add(JSON_CANVAS_HEIGHT, dataManager.getCanvas().getHeight())
		.add(JSON_STATIONS, stationsJSONArray)
                .add(JSON_LINES, linesJSONArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException { //TODO
	// CLEAR THE OLD DATA OUT
	metroData dataManager = (metroData)data;
        dataManager.getChildList().clear();
	dataManager.resetData();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
	// LOAD THE BACKGROUND COLOR
	Color bgColor = loadColor(json, JSON_BG_COLOR);
	dataManager.setBackgroundColor(bgColor);
        
        HashMap<Long, Station> stationIDs = new HashMap<>();
	
	// AND NOW LOAD ALL THE STATIONS
	JsonArray jsonStationArray = json.getJsonArray(JSON_STATIONS);
	for (int i = 0; i < jsonStationArray.size(); i++) {
	    JsonObject jsonStation = jsonStationArray.getJsonObject(i);
	    MarkedStation ms = loadStation(jsonStation);
	    stationIDs.put(ms.getId(), ms.getStation());
	}
        
        Set<MetroLine> lines = new HashSet<>();
        
        //LOAD ALL THE LINES
        ObservableList<Node> childList = dataManager.getChildList();
        
        JsonArray jsonLineArray = json.getJsonArray(JSON_LINES);
	for (int i = 0; i < jsonLineArray.size(); i++) {
	    JsonObject jsonLine = jsonLineArray.getJsonObject(i);
	    MetroLine line = loadLine(jsonLine, stationIDs, childList);
	    lines.add(line);
	}
        
        Set<Station> stations = new HashSet<>(stationIDs.values());
        
        dataManager.addAndResetAllData(stations, lines);
    }
    
    private double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    private String getDataAsString(JsonObject json, String dataName){
        JsonString value = json.getJsonString(dataName);
	String s = value.getString();
        return s;
    }
    
    private long getDataAsLong(JsonObject json, String dataName){
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().longValue();
    }
    
    private boolean getDataAsBoolean(JsonObject json, String dataName){
        boolean value = json.getBoolean(dataName);
        return value;
    }
    
    private Color loadColor(JsonObject json, String colorToGet) {
	JsonObject jsonColor = json.getJsonObject(colorToGet);
	double red = getDataAsDouble(jsonColor, JSON_RED);
	double green = getDataAsDouble(jsonColor, JSON_GREEN);
	double blue = getDataAsDouble(jsonColor, JSON_BLUE);
	double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
	Color loadedColor = new Color(red, green, blue, alpha);
	return loadedColor;
    }
    
    private MarkedStation loadStation(JsonObject stationJson){
        String name = getDataAsString(stationJson, JSON_NAME);
        double x = getDataAsDouble(stationJson, JSON_X);
        double y = getDataAsDouble(stationJson, JSON_Y);
        double radius = getDataAsDouble(stationJson, JSON_RADIUS);
        Color fill = loadColor(stationJson, JSON_FILL_COLOR);
        long id = getDataAsLong(stationJson, JSON_ID);
        
        Color textFill = loadColor(stationJson, JSON_FONT_COLOR);
        String fontFamily = getDataAsString(stationJson, JSON_FONT_FAMILY);
        double fontSize = getDataAsDouble(stationJson, JSON_FONT_SIZE);
        boolean isBold = getDataAsBoolean(stationJson, JSON_BOLD_ATTRIBUTE);
        boolean isItalic = getDataAsBoolean(stationJson, JSON_ITALIC_ATTRIBUTE);
        
        Station station =  new LineNodeBuilder()
                .labelText(name)
                .centerX(x)
                .centerY(y)
                .fillColor(fill)
                .radius(radius)
                .buildStation();
        
        station.label.setBold(isBold);
        station.label.setItalic(isItalic);
        station.label.setFontFamily(fontFamily);
        station.label.setFontSize(fontSize);
        station.label.setFill(textFill);
        
        return new MarkedStation(station, id);
    }
    
    private MetroLine loadLine(JsonObject lineJson, HashMap<Long, Station> stationIDs, ObservableList<Node> childList){
        
        String name     = getDataAsString(lineJson, JSON_NAME);
        double startX   = getDataAsDouble(lineJson, JSON_START_LINE_NODE_X);
        double startY   = getDataAsDouble(lineJson, JSON_START_LINE_NODE_Y);
        double endX     = getDataAsDouble(lineJson, JSON_END_LINE_NODE_X);
        double endY     = getDataAsDouble(lineJson, JSON_END_LINE_NODE_Y);
        Color fillColor = loadColor(lineJson, JSON_FILL_COLOR);
        
        Color textFill = loadColor(lineJson, JSON_FONT_COLOR);
        String fontFamily = getDataAsString(lineJson, JSON_FONT_FAMILY);
        double fontSize = getDataAsDouble(lineJson, JSON_FONT_SIZE);
        boolean isBold = getDataAsBoolean(lineJson, JSON_BOLD_ATTRIBUTE);
        boolean isItalic = getDataAsBoolean(lineJson, JSON_ITALIC_ATTRIBUTE);
        
        double lineThickness = getDataAsDouble(lineJson, JSON_OUTLINE_THICKNESS);
        
        //Load up all the stations
        List<Long> stationIDArray = loadStationIDs(lineJson, JSON_STATION_ARRAY);
        List<Station> stations = new ArrayList<>();
        for(Long id: stationIDArray){
            stations.add(stationIDs.get(id));
        }
        
        //Build the line
        MetroLine line = new MetroLine(name, fillColor, childList);
        line.setStartLocation((int)startX, (int)startY);
        line.setEndLocation((int)endX, (int)endY);
        line.setAllNewStations(stations.toArray(new Station[0]));
        
        line.getStartNode().label.setBold(isBold);
        line.getStartNode().label.setItalic(isItalic);
        line.getStartNode().label.setFontFamily(fontFamily);
        line.getStartNode().label.setFontSize(fontSize);
        line.getStartNode().label.setFill(textFill);
        
        line.getEndNode().label.setBold(isBold);
        line.getEndNode().label.setItalic(isItalic);
        line.getEndNode().label.setFontFamily(fontFamily);
        line.getEndNode().label.setFontSize(fontSize);
        line.getEndNode().label.setFill(textFill);
        
        line.setLineThickness(lineThickness);
        
        return line;
    }
    
    private List<Long> loadStationIDs(JsonObject lineJson, String arrName){
        List<Long> idList = new ArrayList<>();
        JsonArray jsonIdArray = lineJson.getJsonArray(arrName);
        for (int i = 0; i < jsonIdArray.size(); i++) {
	    JsonObject jsonId = jsonIdArray.getJsonObject(i);
	    long id = getDataAsLong(jsonId, JSON_ID);
            idList.add(id);
	}
        return idList;
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    
    
    /**
     * This method is called to export a png image and stupidly formatted json file
     * @param data DataComponenet of application
     * @param filePath to work folder
     * @throws IOException 
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        metroData dataManager = (metroData)data;
        processSnapshot(dataManager, filePath);
        exportWebJSON(dataManager, filePath);
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// AGAIN, WE ARE NOT USING THIS IN THIS ASSIGNMENT
    }
    
    
    private JsonObject makeJsonColorObject(Color color) {
	JsonObject colorJson = Json.createObjectBuilder()
		.add(JSON_RED, color.getRed())
		.add(JSON_GREEN, color.getGreen())
		.add(JSON_BLUE, color.getBlue())
		.add(JSON_ALPHA, color.getOpacity()).build();
	return colorJson;
    }
    
    private JsonObject makeJsonStationObject(Station station, long id){
        JsonObject stationJson = Json.createObjectBuilder()
		    .add(JSON_TYPE, STATION_TYPE)
                    .add(JSON_NAME, station.toString())
		    .add(JSON_X, station.getCenterX())
		    .add(JSON_Y, station.getCenterY())
                    .add(JSON_FONT_FAMILY, station.label.getFontFamily())       //NEW!
                    .add(JSON_FONT_SIZE, station.label.getFontSize())      //NEW!
                    .add(JSON_ITALIC_ATTRIBUTE, station.label.isItalic())       //NEW!
                    .add(JSON_BOLD_ATTRIBUTE, station.label.isBold())                //NEW!
                    .add(JSON_FONT_ORIENTATION, station.label.getOrientation().toString())
                    .add(JSON_FONT_COLOR,  makeJsonColorObject((Color)station.label.getFill()))
		    .add(JSON_RADIUS, station.getRadiusX())
		    .add(JSON_FILL_COLOR, makeJsonColorObject((Color)station.getFill()))
		    //.add(JSON_OUTLINE_COLOR, makeJsonColorObject((Color)station.getStroke()))
		    //.add(JSON_OUTLINE_THICKNESS, station.getStrokeWidth())
                    .add(JSON_ID, id)
                .build();
        return stationJson;
    }
    
    private JsonObject makeJsonLineObject(MetroLine line, HashMap<Station, Long> stationIDs){
        //FIRST BUILD STATION ARRAYS
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        for(Station s: line.getStations()){
            JsonObject stationIdJSON = Json.createObjectBuilder()
                    .add(JSON_ID, stationIDs.get(s))
                    .build();
            jsonArrayBuilder.add(stationIdJSON);
        }
        JsonArray stationsArrayJson = jsonArrayBuilder.build();
        
        //THEN BUILD LINE OBJECT
        JsonObject metroLineJson = Json.createObjectBuilder()
		    .add(JSON_NAME, line.getName())
                    .add(JSON_FILL_COLOR, makeJsonColorObject(line.getColor()))
                    .add(JSON_START_LINE_NODE_X, line.getStartLocationX())
                    .add(JSON_START_LINE_NODE_Y, line.getStartLocationY())
                    .add(JSON_END_LINE_NODE_X, line.getEndLocationX())
                    .add(JSON_END_LINE_NODE_Y, line.getEndLocationY())
                    .add(JSON_OUTLINE_THICKNESS, line.getLineThickness())                   //NEW!
                    .add(JSON_FONT_FAMILY, line.getStartNode().label.getFontFamily())       //NEW!
                    .add(JSON_FONT_SIZE, line.getStartNode().label.getFontSize())      //NEW!
                    .add(JSON_ITALIC_ATTRIBUTE, line.getStartNode().label.isItalic())       //NEW!
                    .add(JSON_BOLD_ATTRIBUTE, line.getStartNode().label.isBold())                //NEW!
                    .add(JSON_FONT_ORIENTATION, line.getStartNode().label.getOrientation().toString())  //NEW!
                    .add(JSON_FONT_COLOR,  makeJsonColorObject((Color)line.getStartNode().label.getFill()))
                    .add(JSON_STATION_ARRAY, stationsArrayJson)
                    
                .build();
        return metroLineJson;
    }
    
    public void processSnapshot(metroData data, String filePath) {
        
        //CLEAN UP FILE PATH TO EXTRACT NAME
        String newPath = filePath.replaceAll(".met$", "") + ".png";
        
        //EXPORT THE PHOTO
	Pane canvasStack = ((metroWorkspace)app.getWorkspaceComponent()).getCanvasStack();
	WritableImage image = canvasStack.snapshot(new SnapshotParameters(), null);
	File file = new File(newPath);
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
	}
    }

    private void exportWebJSON(metroData dataManager, String filePath) throws IOException{
        
        //CLEAN UP FILE PATH TO EXTRACT NAME
        int li = filePath.lastIndexOf("\\");
        String name = filePath.substring(li+1, filePath.length());
        name = name.replaceAll(".met$", "");
        
        JsonArray linesWebArray = makeWebLinesArray(dataManager);
        JsonArray stationsWebArray = makeWebStationsArray(dataManager);
        
        // THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(WEB_NAME, name)
		.add(WEB_LINES, linesWebArray)
                .add(WEB_STATIONS, stationsWebArray)
		.build();
	
        String newPath = filePath.replaceAll(".met$", "") + ".json";
        
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(newPath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(newPath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    /**
     * Helper method for the exportWebJSON function
     * @param dataManager
     * @return returns a formatted json array with lines info
     */
    private JsonArray makeWebLinesArray(metroData dataManager) {
        JsonArrayBuilder linesArrayBuilder = Json.createArrayBuilder();
        for(MetroLine line: dataManager.getMetroLines()){
            linesArrayBuilder.add(makeWebLineJson(line));
        }
        return linesArrayBuilder.build();
    }
    
    private JsonObject makeWebLineJson(MetroLine line){
        JsonObjectBuilder lineJsonBuilder = Json.createObjectBuilder();
        lineJsonBuilder.add(WEB_NAME, line.getName())
                .add(WEB_CIRCULAR, false)
                .add(WEB_COLOR, makeJsonColorObject(line.getColor()))
                .add(WEB_STATION_NAMES, makeWebStationNamesArray(line.getStations()));
        return lineJsonBuilder.build();
    }

    private JsonArray makeWebStationNamesArray(List<Station> stations){
        JsonArrayBuilder stationsArrayBuilder = Json.createArrayBuilder();
        for(Station s: stations){
            stationsArrayBuilder.add(s.label.getText());
        }
        return stationsArrayBuilder.build();
    }
    /**
     * Helper method for the exportWebJSON function
     * @param dataManager
     * @return returns a formatted json array with stations info
     */
    private JsonArray makeWebStationsArray(metroData dataManager) {
        JsonArrayBuilder stationsArrayBuilder = Json.createArrayBuilder();
        for(Station s: dataManager.getStations()){
            stationsArrayBuilder.add(makeWebStation(s));
        }
        return stationsArrayBuilder.build();
    }
    
    private JsonObject makeWebStation(Station s){
        JsonObject stationWeb= Json.createObjectBuilder()
                .add(WEB_NAME, s.label.getText())
                .add(WEB_X, s.getCenterX())
                .add(WEB_Y, s.getCenterY())
                .build();
        return stationWeb;
    }
}
