/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.gui;

import djf.AppTemplate;
import djf.ui.AppDialogs;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import metro.data.DraggableImage;
import metro.data.DraggableText;
import metro.data.LineNode;
import metro.data.MetroLabel;
import metro.data.MetroLine;
import metro.data.Station;
import metro.data.metroData;
import metro.data.metroState;
import static metro.gui.PopupFactory.Popup.*;
import metro.transactions.PaneTransaction;
import metro.transactions.RestorableTransaction;

/**
 *
 * @author Dan
 */
public class metroEditController {
    
    AppTemplate app;
    metroData dataManager;
    metroWorkspace workspace;
    
    PopupFactory popupFactory;
    
    double zoomScale;
    
    public metroEditController(AppTemplate initApp, metroWorkspace workspace) {
	this.app = initApp;
	this.dataManager = (metroData)app.getDataComponent();
        this.workspace = workspace;
        popupFactory = new PopupFactory();
        zoomScale = 1d;
        
    }
    
    public void processAddMetroLine(){
        
        TextInputDialog dialog = popupFactory.getPopup(ADD_LINE_POPUP);
        ((ColorPickerCircle)dialog.getGraphic()).setColor(workspace.lineColorPicker.getColor());
        Optional<String> result = dialog.showAndWait();

        if (result.isPresent() && !result.get().equals("")){
            String lineName = result.get();
            dataManager.addLine(lineName, ((ColorPickerCircle)dialog.getGraphic()).getColor());
        }
    }
    
    public void processAddStationToLine(){
        Scene scene = app.getGUI().getPrimaryScene();
        scene.getRoot().setCursor(Cursor.CROSSHAIR);
        dataManager.setState(metroState.ADDING_STATION_TO_LINE);
    }
    
    public void processRemoveStationFromLine(){
        Scene scene = app.getGUI().getPrimaryScene();
        scene.getRoot().setCursor(Cursor.CROSSHAIR);
        dataManager.setState(metroState.REMOVING_STATION_FROM_LINE);
    }
    
    public void processRemoveMetroLine(){
        String promptTitle = "Remove Line Warning";
        String promptContent = "Are you sure you want to remove this station?";
        
        MetroLine line = workspace.metroLineComboBox.getValue();
        if(line != null && showYesNo(promptTitle, promptContent)){
            dataManager.removeLine(line);
        }
    }
    
    public void processAddStation(){
        Scene scene = app.getGUI().getPrimaryScene();
        dataManager.setState(metroState.PLACING_STATION);
        scene.getRoot().setCursor(Cursor.CROSSHAIR);
    }
    
    public void proccessRemoveStation(){
        String promptTitle = "Remove Station Warning";
        String promptContent = "Are you sure you want to remove this station?";
        
        Station s = workspace.metroStationComboBox.getValue();
        if(s != null && showYesNo(promptTitle, promptContent)){
            dataManager.removeStation(s);
        }
    }
    
    public void processSnapToGrid(){
        Station station = workspace.metroStationComboBox.getValue();
        if(station != null)
            dataManager.snapStationToGrid(station);
    }
    
    public void processLineThickness(){
        double lineThickness = workspace.lineSlider.getValue();
        MetroLine line = workspace.metroLineComboBox.getValue();
        if(line != null)
            dataManager.setLineThickness(lineThickness, line);
    }
    
    void processStationSizeChange() {
        double lineThickness = workspace.stationSlider.getValue();
        Station station = workspace.metroStationComboBox.getValue();
        if(station != null)
            dataManager.setStationSize(lineThickness, station);
    }
    
    public void processMoveLabel(){
        Station s = workspace.metroStationComboBox.getValue();
        dataManager.moveLabel(s);
    }
    
    public void processRotateLabel(){
        
        Station s = workspace.metroStationComboBox.getValue();
        dataManager.toggleLabelOrientation(s);
    }

    void processLineEdit() {
        MetroLine line = workspace.metroLineComboBox.getSelectionModel().getSelectedItem();
        
        if(line == null){
            return;
        }
        
        TextInputDialog dialog = popupFactory.getPopup(EDIT_LINE_POPUP, line);
        ((ColorPickerCircle)dialog.getGraphic()).setColor(workspace.lineColorPicker.getColor());
        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()){
            String lineName = result.get();
            dataManager.changeLineName(line, lineName);
            dataManager.changeLineColor(line, ((ColorPickerCircle)dialog.getGraphic()).getColor());
        }
    }

    void processStationEdit() {
        Station station = workspace.metroStationComboBox.getSelectionModel().getSelectedItem();
        
        if(station == null){
            return;
        }
        
        TextInputDialog dialog = popupFactory.getPopup(EDIT_STATION_POPUP, station);
        ((ColorPickerCircle)dialog.getGraphic()).setColor(workspace.stationColorPicker.getColor());
        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()){
            String stationName = result.get();
            dataManager.changeStationName(station, stationName);
            dataManager.changeStationColor(station, ((ColorPickerCircle)dialog.getGraphic()).getColor());
        }
    }
    
    private boolean showYesNo(String title, String content){
        Alert             confirmationDialog = new Alert(Alert.AlertType.CONFIRMATION);
        confirmationDialog.initOwner(app.getGUI().getWindow());
        confirmationDialog.initModality(Modality.APPLICATION_MODAL);
        confirmationDialog.getButtonTypes().clear();
        confirmationDialog.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);
        confirmationDialog.setTitle(title);
        confirmationDialog.setContentText(content);
        Optional<ButtonType> result = confirmationDialog.showAndWait();
        return result.get().equals(ButtonType.YES);
    }
    
    public void processShowGridToggle(){
        
        boolean selected = workspace.gridCheckBox.isSelected();
        if(selected){
            workspace.canvasStack.getChildren().add(1, workspace.gridLayer);
        }else{
            workspace.canvasStack.getChildren().remove(workspace.gridLayer);
        }
    }
    
    public void processZoomIn(){
        zoomScale += .1;
        workspace.canvasStack.setScaleX(zoomScale);
        workspace.canvasStack.setScaleY(zoomScale);
    }
    
    public void processZoomOut(){
        zoomScale -= .1;
        workspace.canvasStack.setScaleX(zoomScale);
        workspace.canvasStack.setScaleY(zoomScale);
    }
    
    public void processIncreaseCanvasSize(){
        
        Pane canvasStack = workspace.canvasStack;
        PaneTransaction trans = new PaneTransaction(canvasStack);
        
        final double multiplier = 1.1;
        canvasStack.setMinSize(canvasStack.getWidth()*multiplier, canvasStack.getHeight()*multiplier);
        canvasStack.setMaxSize(canvasStack.getWidth()*multiplier, canvasStack.getHeight()*multiplier);
        canvasStack.getParent().layout();
        
        trans.setAfter(canvasStack);
        dataManager.markEdited();
        dataManager.addTransaction(trans);
    }
    
    public void processDecreaseCanvasSize(){
        
        Pane canvasStack = workspace.canvasStack;
        PaneTransaction trans = new PaneTransaction(canvasStack);
        
        final double multiplier = .9;
        canvasStack.setMinSize(canvasStack.getWidth()*multiplier, canvasStack.getHeight()*multiplier);
        canvasStack.setMaxSize(canvasStack.getWidth()*multiplier, canvasStack.getHeight()*multiplier);
        canvasStack.getParent().layout();
        
        trans.setAfter(canvasStack);
        dataManager.markEdited();
        dataManager.addTransaction(trans);
    }

    public void processStationSelected() {
        Station station = workspace.metroStationComboBox.getValue();
        if(station != null)
            workspace.loadSelectedItemSettings(station, dataManager);
    }

    public void processMetroLineSelected() {
        MetroLine line = workspace.metroLineComboBox.getValue();
        if(line != null)
            workspace.loadSelectedItemSettings(line.getStartNode(), dataManager);
    }
    
    public void processShortestRoute(){
        Station toStation = workspace.toStationComboBox.getValue();
        Station fromStation = workspace.fromStationComboBox.getValue();
        if(toStation != null && fromStation != null){
            List<LineNode> stations = dataManager.getRoute(fromStation, toStation);
            if(stations != null){
                
                StringBuilder stationsText = new StringBuilder();
                
                stationsText.append("Origin: ").append(fromStation).append("\n");
                stationsText.append("Destination: ").append(toStation).append("\n");
                stationsText.append("Total Stops: ").append(stations.size()).append("\n");
                stationsText.append("Estimated time: ").append(stations.size()*5).append("\n\n\n");
                
                
                MetroLine previousStationLine = stations.get(0).getFirstLine();
                LinkedHashMap<MetroLine, Double> lineMap = new LinkedHashMap<>();
                
                for(LineNode s: stations){
                    
                }
                
                stationsText.append("Total Route: ").append("\n\n");
                
                for(LineNode s: stations){
                    stationsText.append(s)
                                .append("\n");
                }
                
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Shortest Route");
                alert.setHeaderText("Route from " + fromStation + " to " + toStation);
                alert.setContentText(stationsText.toString());

                alert.showAndWait();
            }else{
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Shortest Route");
                alert.setHeaderText("Path Could Not Be Found");

                alert.showAndWait();
            }
        }
    }
    
    public void processListStationsInLine(){
        MetroLine line = workspace.metroLineComboBox.getValue();
        if(line != null){
            StringBuilder stationsText = new StringBuilder();
            for(Station s: line.getStations()){
                stationsText.append(s)
                            .append("\n");
            }
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Stations");
            alert.setHeaderText("Stations in line: " + line);
            alert.setContentText(stationsText.toString());

            alert.showAndWait();
        }
    }

    public void processAddImage() {
        Scene scene = app.getGUI().getPrimaryScene();
        dataManager.setState(metroState.PLACING_IMAGE);
        scene.getRoot().setCursor(Cursor.CROSSHAIR);
    }

    public void processAddText() {
        Scene scene = app.getGUI().getPrimaryScene();
        dataManager.setState(metroState.PLACING_TEXT);
        scene.getRoot().setCursor(Cursor.CROSSHAIR);
    }
    
    public void processRemoveElement(){
        Node selectedItem = dataManager.getSelectedItem();
        
        if(selectedItem instanceof DraggableImage){
            dataManager.removeImage((DraggableImage)selectedItem);
        }
        else if(selectedItem instanceof DraggableText){
            dataManager.removeText((DraggableText)selectedItem);
        }
                
    }
    public void processBackgroundColorChange() {
        Color color = workspace.backgroundColorPicker.getColor();
        if(color != null){
            dataManager.setBackgroundColor(color);
        }
    }
    public void processBackgroundImage() {
        dataManager.setBackgroundImage();
    }
    
    //          FONT CHANGES HANDLER
    
    
    public void processFontColorChange(){
        
        Node selectedNode = dataManager.getSelectedItem();
        Color color  = workspace.fontColorPicker.getColor();
        
        if(color == null){
            return;
        }
        
        if(selectedNode instanceof Station){
            MetroLabel metLabel = ((Station)selectedNode).label;
            dataManager.changeMetroLabelFontColor(metLabel, color);
        }
        else if(selectedNode instanceof LineNode){
            LineNode endNode = ((LineNode)selectedNode);
            dataManager.changeLineLabelFontColor(endNode, color);
        }
        else if(selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText)selectedNode;
            dataManager.changeDraggableTextColor(text, color);
        }
    }
    
    public void processFontSizeChange(){
        
        Node selectedNode = dataManager.getSelectedItem();
        double size  = workspace.fontSizeComboBox.getValue();
        
        if(selectedNode instanceof Station){
            MetroLabel metLabel = ((Station)selectedNode).label;
            dataManager.changeMetroLabelFontSize(metLabel, size);
        }
        else if(selectedNode instanceof LineNode){
            LineNode endNode = ((LineNode)selectedNode);
            dataManager.changeLineLabelFontSize(endNode, size);
        }
        else if(selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText)selectedNode;
            dataManager.changeDraggableTextSize(text, size);
        }
    }
        
    public void processFontFamilyChange(){
        
        Node selectedNode = dataManager.getSelectedItem();
        String fontFamily  = workspace.fontFamilyComboBox.getValue();
        
        if(fontFamily == null){
            return;
        }
        
        if(selectedNode instanceof Station){
            MetroLabel metLabel = ((Station)selectedNode).label;
            dataManager.changeMetroLabelFontFamily(metLabel, fontFamily);
        }
        else if(selectedNode instanceof LineNode){
            LineNode endNode = ((LineNode)selectedNode);
            dataManager.changeLineLabelFontFamily(endNode, fontFamily);
        }
        else if(selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText)selectedNode;
            dataManager.changeDraggableTextFamily(text, fontFamily);
        }
    }
    
    public void processFontBoldChange(){
        
        Node selectedNode = dataManager.getSelectedItem();
        boolean isBold  = workspace.boldToggle.isSelected();
        
        if(selectedNode instanceof Station){
            MetroLabel metLabel = ((Station)selectedNode).label;
            dataManager.changeMetroLabelFontBold(metLabel, isBold);
        }
        else if(selectedNode instanceof LineNode){
            LineNode endNode = ((LineNode)selectedNode);
            dataManager.changeLineLabelFontBold(endNode, isBold);
        }
        else if(selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText)selectedNode;
            dataManager.changeDraggableTextBold(text, isBold);
        }
    }

    public void processFontItalicChange(){
        
        Node selectedNode = dataManager.getSelectedItem();
        boolean isItalic  = workspace.italicToggle.isSelected();
        
        if(selectedNode instanceof Station){
            MetroLabel metLabel = ((Station)selectedNode).label;
            dataManager.changeMetroLabelFontItalic(metLabel, isItalic);
        }
        else if(selectedNode instanceof LineNode){
            LineNode endNode = ((LineNode)selectedNode);
            dataManager.changeLineLabelFontItalic(endNode, isItalic);
        }
        else if(selectedNode instanceof DraggableText){
            DraggableText text = (DraggableText)selectedNode;
            dataManager.changeDraggableTextItalic(text, isItalic);
        }
    }

}
