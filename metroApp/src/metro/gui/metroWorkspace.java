package metro.gui;

import java.io.IOException;
import javafx.scene.layout.Pane;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import static metro.css.metroStyle.CLASS_EDIT_TOOLBAR;
import static metro.css.metroStyle.CLASS_EDIT_TOOLBAR_ROW;
import static metro.css.metroStyle.CLASS_HGROW_COMBO;
import static metro.css.metroStyle.CLASS_RENDER_CANVAS;
import static metro.css.metroStyle.CLASS_TOOL_LABEL;
import metro.data.LineNode;
import metro.data.MetroLine;
import metro.data.Station;
import metro.data.metroData;
import metro.data.Connection;
import static metro.metroPropertyType.*;
import properties_manager.PropertiesManager;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Daniel Baccarella
 * @author ?
 * @version 1.0
 */
public class metroWorkspace extends AppWorkspaceComponent {
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    //We will use this to stack the grid on top of the gui pane
    StackPane canvasStack;
    
    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    Pane canvas;
    
    GridLayer gridLayer;
    
    Pane backgroundLayer;
    
    ScrollPane canvasScrollPane;
    
    //We will put all the toolbars into this toolbar pane
    VBox toolbar;
    
    ToolGroup tg1;
    Label metroLinesLabel;
    ComboBox<MetroLine> metroLineComboBox;
    public ColorPickerCircle lineColorPicker;
    Button linePlusButton;
    Button lineMinusButton;
    Button addStationButton;
    Button removeStationButton;
    Button listLineButton;
    Slider lineSlider;
    
    ToolGroup tg2;
    Label metroStationLabel;
    ComboBox<Station> metroStationComboBox;
    public ColorPickerCircle stationColorPicker;
    Button stationPlusButton;
    Button stationMinusButton;
    Button snapButton;
    Button moveLabelButton;
    Button rotateButton;
    Slider stationSlider;
    
    ToolGroup tg3;
    ComboBox<Station> fromStationComboBox;
    ComboBox<Station> toStationComboBox;
    Button checkRouteButton;
    
    ToolGroup tg4;
    Label decorLabel;
    ColorPickerCircle backgroundColorPicker;
    Button backgroundImageButton;
    Button addImageButton;
    Button addLabelButton;
    Button removeElementButton;
    
    ToolGroup tg5;
    Label fontLabel;
    ColorPickerCircle fontColorPicker;
    public ToggleButton boldToggle;
    public ToggleButton italicToggle;
    public ComboBox<Integer> fontSizeComboBox;
    public ComboBox<String> fontFamilyComboBox;
    
    ToolGroup tg6;
    Label navigationLabel;
    CheckBox gridCheckBox;
    CheckBox snapToggle;
    Label showGridLabel;
    Label snapToggleLabel;
    Button zoomInButton;
    Button zoomOutButton;
    Button shrinkButton;
    Button expandButton;
    
    
    //*****
    metroEditController editController;
    CanvasController canvasController;
          
    
    
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public metroWorkspace(AppTemplate initApp) {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        //Set up canvas
        canvas = new Pane();
        
        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }
    
    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    

    public Pane getCanvas() {
	return canvas;
    }
        
    // HELPER SETUP METHOD
    private void initLayout() {
        // WE'LL USE THIS TO GET TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //We will use this to make buttons
        AppGUI appGui = app.getGUI();
        //ToolGroups initialization
        tg1                     = new ToolGroup(3);
        metroLinesLabel         = new Label("Metro Lines");
        metroLineComboBox       = new ComboBox<>();
        lineColorPicker         = new ColorPickerCircle(20, false);
        linePlusButton          = new Button("+");
        lineMinusButton         = new Button("-");
        addStationButton        = new Button("Add\nStation");
        removeStationButton     = new Button("Remove\nStation");
        listLineButton          = appGui.makeButton(LIST_ICON.toString(), LIST_TOOLTIP.toString(), true);
        lineSlider              = new Slider(.5, 10, 1);

        tg2                     = new ToolGroup(3);
        metroStationLabel       = new Label("Metro Stations");
        metroStationComboBox    = new ComboBox<>();
        stationColorPicker      = new ColorPickerCircle(20, false);
        stationPlusButton       = new Button("+");
        stationMinusButton      = new Button("-");
        snapButton              = new Button("Snap");
        moveLabelButton         = new Button("Move Label");
        rotateButton            = appGui.makeButton(ROTATE_ICON.toString(), ROTATE_TOOLTIP.toString(), true);
        stationSlider           = new Slider(5, 30, 15);

        tg3                     = new ToolGroup(2);
        fromStationComboBox     = new ComboBox();
        toStationComboBox       = new ComboBox();
        checkRouteButton        = appGui.makeButton(DISTANCE_LARGE_ICON.toString(), DISTANCE_TOOLTIP.toString(), true);

        tg4                     = new ToolGroup(2);
        decorLabel              = new Label("Decor");
        backgroundColorPicker   = new ColorPickerCircle(20);
        backgroundImageButton   = new Button("Set Background\nImage");
        addImageButton          = new Button("Add\nImage");
        addLabelButton          = new Button("Add\nLabel");
        removeElementButton       = new Button("Remove\nElement");

        tg5                     = new ToolGroup(2);
        fontLabel               = new Label("Font");
        fontColorPicker         = new ColorPickerCircle(20);
        boldToggle              = new ToggleButton("B");
        italicToggle            = new ToggleButton("I");
        fontSizeComboBox        = new ComboBox();
        fontFamilyComboBox      = new ComboBox();

        tg6                     = new ToolGroup(2);
        navigationLabel         = new Label("Navigation");
        snapToggle              = new CheckBox();
        snapToggleLabel         = new Label("Snap to Grid");
        gridCheckBox            = new CheckBox();
        showGridLabel           = new Label("Show Grid");
        zoomInButton            = appGui.makeButton(ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), true);
        zoomOutButton           = appGui.makeButton(ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), true);
        shrinkButton            = appGui.makeButton(SHRINK_ICON.toString(), SHRINK_TOOLTIP.toString(), true);
        expandButton            = appGui.makeButton(EXPAND_ICON.toString(), EXPAND_TOOLTIP.toString(), true);
        
        //workspace initialization
        workspace               = new BorderPane();
        toolbar                 = new VBox();
        canvasStack             = new StackPane();
        canvas                  = new Pane();
        gridLayer               = new GridLayer(20);
        backgroundLayer         = new Pane();
        
        //set canvas as base for canvas stack
        canvasStack.getChildren().addAll(backgroundLayer, canvas);
        
        canvasScrollPane = new ScrollPane();
        canvasScrollPane.setFitToHeight(true);
        canvasScrollPane.setFitToWidth(true);
        canvasScrollPane.setContent(new Group(canvasStack));
        
        canvasStack.prefWidthProperty().bind(canvasScrollPane.widthProperty());
        canvasStack.prefHeightProperty().bind(canvasScrollPane.heightProperty());
        
        //set up the workspace area
        ((BorderPane)workspace).setCenter(canvasScrollPane);
        ((BorderPane)workspace).setLeft(toolbar);
        Node top = ((BorderPane)workspace).getTop();
        ((BorderPane)workspace).setTop(null);
        ((BorderPane)workspace).setTop(top);
        
        
        
        //set up the layout for all the toolgroups
        tg1.addAllElements(0, metroLinesLabel, metroLineComboBox, lineColorPicker);
        tg1.addAllElements(1, linePlusButton, lineMinusButton, addStationButton, removeStationButton, listLineButton);
        tg1.addAllElements(2, lineSlider);
        
        tg2.addAllElements(0, metroStationLabel, metroStationComboBox, stationColorPicker);
        tg2.addAllElements(1, stationPlusButton, stationMinusButton, snapButton, moveLabelButton, rotateButton);
        tg2.addAllElements(2, stationSlider);
        
        tg3.addElement(0, fromStationComboBox);
        tg3.addElement(1, toStationComboBox);
        tg3.setRight(checkRouteButton);
        
        tg4.addAllElements(0, decorLabel, backgroundColorPicker);
        tg4.addAllElements(1, backgroundImageButton, addImageButton, addLabelButton, removeElementButton);
        
        tg5.addAllElements(0, fontLabel, fontColorPicker);
        tg5.addAllElements(1, boldToggle, italicToggle, fontSizeComboBox, fontFamilyComboBox);
        
        tg6.addAllElements(0, navigationLabel, snapToggle, snapToggleLabel, gridCheckBox, showGridLabel);
        tg6.addAllElements(1, zoomInButton, zoomOutButton, shrinkButton, expandButton);
        
        toolbar.getChildren().addAll(tg1, tg2, tg3, tg4, tg5, tg6);
        
        //Hook the list into the data controller
        metroData data = (metroData)app.getDataComponent();
	data.setCanvas(canvas, backgroundLayer, gridLayer);
    }
    
    // HELPER SETUP METHOD
    private void initControllers() {
        
        editController = new metroEditController(app, this);
        canvasController = new CanvasController(app);
        
        //this will fire when the user tries to close the window
	app.getGUI().getWindow().setOnCloseRequest( e->{
            //Consume the window close event so it doesnt close on cancel
            e.consume();
            //we will just use the exit request in the desktopjava framework
            app.getGUI().getFileController().processExitRequest();
        });
        
        
        //SET UP BUTTON ACTIONS
        
        linePlusButton.setOnAction(e ->{
            editController.processAddMetroLine();
        });
        
        listLineButton.setOnAction(e ->{
            editController.processListStationsInLine();
        });
        
        stationPlusButton.setOnAction(e ->{
            editController.processAddStation();
        });
        
        addStationButton.setOnAction(e ->{
            editController.processAddStationToLine();
        });
        removeStationButton.setOnAction(e ->{
            editController.processRemoveStationFromLine();
        });
        rotateButton.setOnAction(e ->{
           editController.processRotateLabel();
        });
        moveLabelButton.setOnAction(e ->{
           editController.processMoveLabel();
        });
        snapButton.setOnAction(e ->{
            editController.processSnapToGrid();
        });
        lineMinusButton.setOnAction(e ->{
           editController.processRemoveMetroLine();
        });
        stationMinusButton.setOnAction(e ->{
            editController.proccessRemoveStation();
        });
        
        checkRouteButton.setOnAction(e ->{
            editController.processShortestRoute();
        });
        
        gridCheckBox.setOnAction(e ->{
            editController.processShowGridToggle();
        });
        
        addImageButton.setOnAction(e ->{
            editController.processAddImage();
        });
        
        addLabelButton.setOnAction(e ->{
            editController.processAddText();
        });
        
        removeElementButton.setOnAction(e ->{
            editController.processRemoveElement();
        });
        
        backgroundColorPicker.colorPicker.setOnAction(e ->{
            editController.processBackgroundColorChange();
        });
        
        backgroundImageButton.setOnAction(e ->{
            editController.processBackgroundImage();
        });
        
        fontColorPicker.colorPicker.setOnAction(e ->{
            editController.processFontColorChange();
        });
        
        zoomInButton.setOnAction(e ->{
            editController.processZoomIn();
        });
        zoomOutButton.setOnAction(e ->{
            editController.processZoomOut();
        });
        expandButton.setOnAction(e ->{
            editController.processIncreaseCanvasSize();
        });
        shrinkButton.setOnAction(e ->{
            editController.processDecreaseCanvasSize();
        });
        metroStationComboBox.setOnAction(e->{
            editController.processStationSelected();
        });
        metroLineComboBox.setOnAction(e->{
            editController.processMetroLineSelected();
        });
        
        lineSlider.valueProperty().addListener(e-> {
            editController.processLineThickness();
	});
        stationSlider.valueProperty().addListener(e ->{
            editController.processStationSizeChange();
        });
        
        lineColorPicker.setOnMouseClicked(e ->{
            e.consume();
            editController.processLineEdit();
        });
        stationColorPicker.setOnMouseClicked(e ->{
            e.consume();
            editController.processStationEdit();
        });
        
        boldToggle.setOnAction(e ->{
            editController.processFontBoldChange();
        });
        italicToggle.setOnAction(e ->{
            editController.processFontItalicChange();
        });
        fontSizeComboBox.setOnAction(e ->{
            editController.processFontSizeChange();
        });
        fontFamilyComboBox.setOnAction(e ->{
            editController.processFontFamilyChange();
        });
        
        //HOOK THE CANVAS CONTROLLER
        canvas.setOnMousePressed(e->{
            canvasController.processCanvasMousePress((int)e.getX(), (int)e.getY());
            if(e.getClickCount() == 2){
                canvasController.processCanvasDoubleClick((int)e.getX(), (int)e.getY());
            }
	});
	canvas.setOnMouseReleased(e->{
	    canvasController.processCanvasMouseRelease((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseDragged(e->{
	    canvasController.processCanvasMouseDragged((int)e.getX(), (int)e.getY());
	});
        
        app.getGUI().getWindow().getScene().setOnKeyPressed((KeyEvent event) ->{
            canvasController.processKeyPress(event);
        });
    }


    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamically as the application
     * runs so they will have their style setup separately.
     */
    public final void initStyle() {
        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
        
        //toolbar style
        toolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        tg1.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        tg2.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        tg3.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        tg4.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        tg5.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        tg6.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        
        //place spacers into toolbars where needed
        tg4.insertSpacer(0, 1);
        tg5.insertSpacer(0, 1);
        tg6.insertSpacer(0, 1);
        
        //Add hgrow to items that need it
        HBox.setHgrow(metroLineComboBox, Priority.ALWAYS);
        HBox.setHgrow(metroStationComboBox, Priority.ALWAYS);
        HBox.setHgrow(fromStationComboBox, Priority.ALWAYS);
        HBox.setHgrow(toStationComboBox, Priority.ALWAYS);
        HBox.setHgrow(fontFamilyComboBox, Priority.ALWAYS);
        HBox.setHgrow(lineSlider, Priority.ALWAYS);
        HBox.setHgrow(stationSlider, Priority.ALWAYS);
        metroLineComboBox.getStyleClass().add(CLASS_HGROW_COMBO);
        metroStationComboBox.getStyleClass().add(CLASS_HGROW_COMBO);
        fromStationComboBox.getStyleClass().add(CLASS_HGROW_COMBO);
        toStationComboBox.getStyleClass().add(CLASS_HGROW_COMBO);
        fontFamilyComboBox.getStyleClass().add(CLASS_HGROW_COMBO);
        
        //Labels for classes
        metroLinesLabel.getStyleClass().add(CLASS_TOOL_LABEL);
        metroStationLabel.getStyleClass().add(CLASS_TOOL_LABEL);
        decorLabel.getStyleClass().add(CLASS_TOOL_LABEL);
        fontLabel.getStyleClass().add(CLASS_TOOL_LABEL);
        navigationLabel.getStyleClass().add(CLASS_TOOL_LABEL);
        
        BorderPane.setMargin(checkRouteButton, new Insets(0,0,0,10));
        
        metroStationComboBox.setCellFactory((ListView<Station> p) -> {
            return new ListCell<Station>(){
                @Override protected void updateItem(Station item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty && item != null) {
                        setText(item.toString());
                        setGraphic(null);
                    } else {
                        setText(null);
                    }
                }
                
            };
        });
        
        metroStationComboBox.setButtonCell(new ListCell<Station>(){
                @Override protected void updateItem(Station item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty && item != null) {
                        setText(item.toString());
                        setGraphic(null);
                    } else {
                        setText(null);
                    }
                }       
        });
        
        toStationComboBox.setCellFactory((ListView<Station> p) -> {
            return new ListCell<Station>(){
                @Override protected void updateItem(Station item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty && item != null) {
                        setText(item.toString());
                        setGraphic(null);
                    } else {
                        setText(null);
                    }
                }
                
            };
        });
        
        toStationComboBox.setButtonCell(new ListCell<Station>(){
                @Override protected void updateItem(Station item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty && item != null) {
                        setText(item.toString());
                        setGraphic(null);
                    } else {
                        setText(null);
                    }
                }       
        });
        
        fromStationComboBox.setCellFactory((ListView<Station> p) -> {
            return new ListCell<Station>(){
                @Override protected void updateItem(Station item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty && item != null) {
                        setText(item.toString());
                        setGraphic(null);
                    } else {
                        setText(null);
                    }
                }
                
            };
        });
        
        fromStationComboBox.setButtonCell(new ListCell<Station>(){
                @Override protected void updateItem(Station item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty && item != null) {
                        setText(item.toString());
                        setGraphic(null);
                    } else {
                        setText(null);
                    }
                }       
        });
        
        fontSizeComboBox.getItems().addAll(IntStream.range(6, 151)
                                                    .filter(x -> x % 3 == 0)
                                                    .boxed()
                                                    .collect(Collectors.toList()));
        
        fontSizeComboBox.setValue(21);
        
        fontFamilyComboBox.getItems().addAll(Font.getFamilies());
        fontFamilyComboBox.setValue("Arial");
        
        fontFamilyComboBox.setCellFactory((ListView<String> param) -> {
            ListCell<String> cell = new ListCell<String>() {
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item);
                        setFont(Font.font(item));
                    }
                }
            };
            return cell;
        });
        
        
    }

    /**
     * This function reloads all the controls for editing logos in
     * the workspace.
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
	metroData dataManager = (metroData)data;
	
        //RELOAD COMBO BOXES
       Station selectedStation = metroStationComboBox.getValue();
       MetroLine selectedLine = metroLineComboBox.getValue();
        
       metroStationComboBox.getItems().clear();
       metroStationComboBox.getItems().addAll(dataManager.getStations().stream().sorted().collect(Collectors.toList()));
       if(selectedStation != null && dataManager.getStations().contains(selectedStation))
            metroStationComboBox.getSelectionModel().select(selectedStation);
       metroLineComboBox.getItems().clear();
       metroLineComboBox.getItems().addAll(dataManager.getMetroLines());
       if(selectedLine != null && dataManager.getMetroLines().contains(selectedLine))
            metroLineComboBox.getSelectionModel().select(selectedLine);
       fromStationComboBox.getItems().clear();
       fromStationComboBox.getItems().addAll(dataManager.getStations().stream().sorted().collect(Collectors.toList()));
       toStationComboBox.getItems().clear();
       toStationComboBox.getItems().addAll(dataManager.getStations().stream().sorted().collect(Collectors.toList()));
       
       
       //Load Combobox's with selected items
       Node selectedItem = dataManager.getSelectedItem();
       if(selectedItem != null){
           loadSelectedItemSettings(selectedItem, dataManager);
       }
    }
    
    @Override
    public void resetLanguage() {
        // WE'LL NEED TO RELOAD THE CONTROLS WITH TEXT
        // THAT ARE NOT BUTTONS HERE, LIKE LABELS AND COMBO BOXES
        
    }

    public void loadSelectedItemSettings(Node selectedItem, metroData dataManager) {
        
        if(selectedItem.getClass().equals(LineNode.class)){
               LineNode lineEnd = (LineNode)selectedItem;
               MetroLine line = lineEnd.getFirstLine();
               if(dataManager.getMetroLines().contains(line)){
                   
                   fontSizeComboBox.setDisable(true);
                   fontFamilyComboBox.setDisable(true);
                   boldToggle.setDisable(true);
                   italicToggle.setDisable(true);
                   
                   metroLineComboBox.getSelectionModel().select(line);
                   lineColorPicker.colorPicker.setValue(line.getColor());
                   lineSlider.setValue(line.getLineThickness());
                   /*
                   fontFamilyComboBox.setValue(line.getStartNode().label.getFontFamily());
                   fontSizeComboBox.setValue((int)line.getStartNode().label.getFontSize());
                   boldToggle.setSelected(line.getStartNode().label.isBold());
                   italicToggle.setSelected(line.getStartNode().label.isItalic());
                   */
                   fontSizeComboBox.setDisable(false);
                   fontFamilyComboBox.setDisable(false);
                   boldToggle.setDisable(false);
                   italicToggle.setDisable(false);
               }
           }else if(selectedItem.getClass().equals(Station.class)){
               Station station = (Station)selectedItem;
               if(dataManager.getStations().contains(station)){
                   
                   fontSizeComboBox.setDisable(true);
                   fontFamilyComboBox.setDisable(true);
                   boldToggle.setDisable(true);
                   italicToggle.setDisable(true);
                   
                   metroStationComboBox.getSelectionModel().select(station);
                   stationColorPicker.colorPicker.setValue((Color)station.getFill());
                   /*
                   fontFamilyComboBox.setValue(station.label.getFontFamily());
                   fontSizeComboBox.setValue((int)station.label.getFontSize());
                   boldToggle.setSelected(station.label.isBold());
                   italicToggle.setSelected(station.label.isItalic());
                   */
                   fontSizeComboBox.setDisable(false);
                   fontFamilyComboBox.setDisable(false);
                   boldToggle.setDisable(false);
                   italicToggle.setDisable(false);
               }
           }else if(selectedItem instanceof Connection){
               Connection connection = (Connection)selectedItem;
               MetroLine line = connection.getLine();
               if(dataManager.getMetroLines().contains(line)){
                   metroLineComboBox.getSelectionModel().select(line);
                   lineColorPicker.colorPicker.setValue(line.getColor());
               }
           }
    }

    public Pane getCanvasStack() {
        return canvasStack;
    }
    

}