/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.gui;

import djf.AppTemplate;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import metro.data.Copyable;
import metro.data.Draggable;
import metro.data.LineNode;
import metro.data.MetroLine;
import metro.data.Station;
import metro.data.metroData;
import static metro.data.metroState.ADDING_STATION_TO_LINE;
import static metro.data.metroState.DRAGGING_ITEM;
import static metro.data.metroState.DRAGGING_NOTHING;
import static metro.data.metroState.PLACING_IMAGE;
import static metro.data.metroState.PLACING_STATION;
import static metro.data.metroState.PLACING_TEXT;
import static metro.data.metroState.REMOVING_STATION_FROM_LINE;
import static metro.data.metroState.SELECTING_ITEM;
import metro.transactions.RestorableTransaction;

/**
 *
 * @author Dan
 */
public class CanvasController {
    AppTemplate app;
    
    
    int keyFrameX = -1;
    int keyFrameY = -1;
    
    boolean shapeDragged;
    RestorableTransaction trans;
    
    //Controlls speed of WASD movements
    double SCROLL_MULTIPLIER = .025;

    public CanvasController(AppTemplate initApp) {
        app = initApp;
    }

    /**
     * Respond to mouse presses on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMousePress(int x, int y) {

        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        metroData dataManager = (metroData) app.getDataComponent();
        
        //disable cut & copy until we get a confimred shape highlight
        //workspace.setCopyDisable(true);
        //workspace.setCutDisable(true);
        
        Scene scene = app.getGUI().getPrimaryScene();
        
        if (dataManager.isInState(SELECTING_ITEM)) {
           processMousePressSelectingItem(x, y);
        } 
        else if (dataManager.isInState(PLACING_STATION)) {
           processMousePressPlacingStation(x, y);
        }
        else if (dataManager.isInState(ADDING_STATION_TO_LINE)){
            processMousePressAddingStationToLine(x, y);
        }
        else if (dataManager.isInState(REMOVING_STATION_FROM_LINE)){
            processMousePressRemovingStationFromLine(x, y);
        }
        else if (dataManager.isInState(PLACING_TEXT)){
            processMousePressPlaceText(x, y);
        }
        else if (dataManager.isInState(PLACING_IMAGE)){
            processMousePressPlaceImage(x, y);
        }
        workspace.reloadWorkspace(dataManager);
    }
    
    public void processCanvasDoubleClick(int x, int y){
        //Todo
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseDragged(int x, int y) {
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        ScrollPane sp = workspace.canvasScrollPane;
        
        metroData dataManager = (metroData) app.getDataComponent();
        Scene scene = app.getGUI().getPrimaryScene();
        if (dataManager.isInState(DRAGGING_ITEM) && workspace.snapToggle.isSelected()){
            scene.getRoot().setCursor(Cursor.MOVE);
            processDragWithSnap(x, y);
        }
        else if(dataManager.isInState(DRAGGING_ITEM)) {
            scene.getRoot().setCursor(Cursor.MOVE);
            processDragNoSnap(x, y);
        }else if(dataManager.isInState(DRAGGING_NOTHING)){
            sp.setPannable(true);
        }
    }

    /**
     * Respond to mouse button release on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseRelease(int x, int y) {
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        ScrollPane sp = workspace.canvasScrollPane;
        Scene scene = app.getGUI().getPrimaryScene();
        
        metroData dataManager = (metroData) app.getDataComponent();
        if (dataManager.isInState(DRAGGING_ITEM)) {
            dataManager.setState(SELECTING_ITEM);
            scene.getRoot().setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
            if(shapeDragged){
                if(dataManager.getSelectedItem() != null && dataManager.getSelectedItem().getClass().equals(LineNode.class)){
                    ((LineNode)dataManager.getSelectedItem()).setFill(Color.TRANSPARENT);
                }
                trans.setAfter((Copyable)dataManager.getSelectedItem());
                dataManager.markEdited();
                dataManager.addTransaction(trans);
                trans = null; //Reset trans for next move
            }
        } else if (dataManager.isInState(DRAGGING_NOTHING)) {
            sp.setPannable(false);
            dataManager.setState(SELECTING_ITEM);
        } else if (dataManager.isInState(PLACING_STATION)){
            dataManager.setState(SELECTING_ITEM);
        } else {
            dataManager.setState(SELECTING_ITEM);
        }
    }
    
    /******************************************************************************************************
     * 
     *                  MOUSE PRESS CONTROL METHODS
     * 
     *******************************************************************************************************/
    
    private void processMousePressSelectingItem(int x, int y){
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        metroData dataManager = (metroData) app.getDataComponent();

        Scene scene = app.getGUI().getPrimaryScene();
         // SELECT THE TOP SHAPE
            Node item = dataManager.selectTopItem(x, y);
            
            keyFrameX = x;
            keyFrameY = y;

            // AND START DRAGGING IT
            if (item != null && item instanceof Draggable) {                
                keyFrameX = x;
                keyFrameY = y;
                
                scene.getRoot().setCursor(Cursor.MOVE);
                dataManager.setState(DRAGGING_ITEM);
                app.getGUI().updateToolbarControls(false);
                shapeDragged = false;
                trans = new RestorableTransaction((Copyable)item);
            } else {
                scene.getRoot().setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
    }
    
    private void processMousePressPlacingStation(int x, int y){
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        metroData dataManager = (metroData) app.getDataComponent();

        Scene scene = app.getGUI().getPrimaryScene();
        
        dataManager.addStation(x, y);
        dataManager.setState(DRAGGING_ITEM);
        scene.getRoot().setCursor(Cursor.DEFAULT);
    }

    private void processMousePressAddingStationToLine(int x, int y) {
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        metroData dataManager = (metroData) app.getDataComponent();

        Scene scene = app.getGUI().getPrimaryScene();
        
        Node item = dataManager.getTopItem(x, y);
        if(item != null && item.getClass().equals(Station.class)){
            Station station = (Station)item;
            MetroLine line = workspace.metroLineComboBox.getValue();
            if(line != null)
                dataManager.addStationToLine(station, line);
        }else{
            //If the click not on a station, just treat it like it was a selection
             processMousePressSelectingItem(x, y);
        }
    }

    private void processMousePressRemovingStationFromLine(int x, int y) {
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        metroData dataManager = (metroData) app.getDataComponent();

        Scene scene = app.getGUI().getPrimaryScene();
        
        Node item = dataManager.getTopItem(x, y);
        if(item != null && item.getClass().equals(Station.class)){
            Station station = (Station)item;
            MetroLine line = workspace.metroLineComboBox.getValue();
            if(line != null)
                dataManager.removeStationFromLine(station, line);
        }else{
            //If the click not on a station, just treat it like it was a selection
             processMousePressSelectingItem(x, y);
        }
    }
    
    private void processMousePressPlaceText(int x, int y) {
       Scene scene = app.getGUI().getPrimaryScene();
       metroData dataManager = (metroData) app.getDataComponent();
       dataManager.addText(x, y);
       dataManager.setState(SELECTING_ITEM);
       scene.getRoot().setCursor(Cursor.DEFAULT);
    }

    private void processMousePressPlaceImage(int x, int y) {
        Scene scene = app.getGUI().getPrimaryScene();
        metroData dataManager = (metroData) app.getDataComponent();
        dataManager.addImage(x, y);
        dataManager.setState(SELECTING_ITEM);
        scene.getRoot().setCursor(Cursor.DEFAULT);
    }
    
    /* ------------------------------------------------------
    *   Drag Controllers
    -----------------------------------------------------------*/

    private void processDragNoSnap(int x, int y){
            
        metroData dataManager = (metroData) app.getDataComponent();
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
            
        Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedItem();
        int diffX = x - keyFrameX;
        int diffY = y - keyFrameY;
            
        if(keyFrameX == -1 || keyFrameY == -1){
            return;
        }
        //if it moves at all it was dragged
        if(x != keyFrameX || y != keyFrameY){
            shapeDragged = true;
            //Check if its an end node
            if(dataManager.getSelectedItem().getClass().equals(LineNode.class)){
                ((LineNode)dataManager.getSelectedItem()).setFill(Color.YELLOW);
            }
        }
            
        selectedDraggableShape.drag(diffX, diffY);
        app.getGUI().updateToolbarControls(false);
        keyFrameX = x;
        keyFrameY = y;
    }
    
    private void processDragWithSnap(int x, int y){
        metroData dataManager = (metroData) app.getDataComponent();
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
            
        Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedItem();
        int diffX = x - keyFrameX;
        int diffY = y - keyFrameY;
            
        if(keyFrameX == -1 || keyFrameY == -1){
            return;
        }
        
        double gridWidth = workspace.gridLayer.getGridWidth();
        
        selectedDraggableShape.snapToGrid(gridWidth);
        //if it moves at all it was dragged
        if(x != keyFrameX || y != keyFrameY){
            shapeDragged = true;
            //Check if its an end node
            if(dataManager.getSelectedItem().getClass().equals(LineNode.class)){
                ((LineNode)dataManager.getSelectedItem()).setFill(Color.YELLOW);
            }
        }
        
        if(Math.abs(diffX) >= gridWidth){
            selectedDraggableShape.drag(diffX, 0);
            selectedDraggableShape.snapToGrid(gridWidth);
            
            app.getGUI().updateToolbarControls(false);
            keyFrameX = x;
            
        }
        if(Math.abs(diffY) >= gridWidth){
            selectedDraggableShape.drag(0, diffY);
            selectedDraggableShape.snapToGrid(gridWidth);
            
            app.getGUI().updateToolbarControls(false);
            keyFrameY = y;
        }
        
    }

    public void processKeyPress(KeyEvent event) {
        switch(event.getCode()){
            case W: shiftCanvasUp();
                break;
            case S: shiftCanvasDown();
                break;
            case A: shiftCanvasLeft();
                break;
            case D: shiftCanvasRight();
                break;
        }
    }

    private void shiftCanvasUp() {
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        ScrollPane sp = workspace.canvasScrollPane;
        sp.setVvalue(sp.getVvalue() - SCROLL_MULTIPLIER);
    }

    private void shiftCanvasDown() {
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        ScrollPane sp = workspace.canvasScrollPane;
        sp.setVvalue(sp.getVvalue() + SCROLL_MULTIPLIER);
    }

    private void shiftCanvasLeft() {
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        ScrollPane sp = workspace.canvasScrollPane;
        sp.setHvalue(sp.getHvalue() - SCROLL_MULTIPLIER);
    }

    private void shiftCanvasRight() {
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        ScrollPane sp = workspace.canvasScrollPane;
        sp.setHvalue(sp.getHvalue() + SCROLL_MULTIPLIER);
    }
}
