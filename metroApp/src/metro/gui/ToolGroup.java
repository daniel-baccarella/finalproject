/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import static metro.css.metroStyle.CLASS_TOOL_CHILD;
import static metro.css.metroStyle.CLASS_TOOL_GROUP;
import static metro.css.metroStyle.CLASS_TOOL_HBOX;
import static metro.css.metroStyle.CLASS_TOOL_VBOX;

/**
 *
 * @author Dan
 */
public class ToolGroup extends BorderPane{
    private List<HBox> rows = new ArrayList<>();
    private VBox rowContainer = new VBox();
    
    public ToolGroup(int numRows){
        //set row cont in center
        this.setCenter(rowContainer);
        //add the num of rows to the rowContainer
        IntStream
                .range(0, numRows)
                .forEach(
                    i ->{
                        HBox hbox = new HBox();
                        hbox.getStyleClass().add(CLASS_TOOL_HBOX);
                        rowContainer.getChildren().add(hbox);
                        rows.add(hbox);
                        }
                    );
        
        this.getStyleClass().add(CLASS_TOOL_GROUP);
        rowContainer.getStyleClass().add(CLASS_TOOL_VBOX);
    }
    
    public void addElement(int row, Node node){
        HBox focusHBox = rows.get(row);
        focusHBox.getChildren().add(node);
        node.getStyleClass().add(CLASS_TOOL_CHILD);
    }
    
    public void addAllElements(int row, Node...nodes){
        HBox focusHBox = rows.get(row);
        focusHBox.getChildren().addAll(nodes);
        for(Node node: nodes){
            node.getStyleClass().add(CLASS_TOOL_CHILD);
        }
    }
    
    public void insertSpacer(int row, int column){
        Pane spacer = new Pane();
        HBox.setHgrow(spacer, Priority.SOMETIMES);
        rows.get(row).getChildren().add(column, spacer);
    }
    
}
