/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.gui;

import java.util.Optional;
import javafx.scene.control.TextInputDialog;
import javafx.scene.paint.Color;
import metro.data.MetroLine;
import metro.data.Station;

/**
 *
 * @author Dan
 */
public class PopupFactory {
    public static enum Popup{
        ADD_STATION_POPUP,
        ADD_LINE_POPUP,
        EDIT_STATION_POPUP,
        EDIT_LINE_POPUP,
        ADD_TEXT_POPUP
    }
    
    public TextInputDialog getPopup(Popup popup){
        return getPopup(popup, null);
    }
    
    public TextInputDialog getPopup(Popup popup, Object obj){
        ColorPickerCircle cp = new ColorPickerCircle(20);
        TextInputDialog dialog = new TextInputDialog();
        dialog.setGraphic(cp);
        
        switch(popup){
            case ADD_STATION_POPUP:
                dialog.setTitle("Add Station");
                dialog.setHeaderText("Metro Station Details");
                dialog.setContentText("Name:");
                break;
            case ADD_LINE_POPUP:
                dialog.setTitle("Add Line");
                dialog.setHeaderText("Metro Line Details");
                dialog.setContentText("Name:");
                break;
            case EDIT_STATION_POPUP:
                dialog.setTitle("Edit Station");
                dialog.setHeaderText("Metro Station Details");
                dialog.setContentText("Name:");
                Station station = (Station)obj;
                dialog.getEditor().setText(station.toString());
                cp.setColor((Color) station.getFill());
                break;
            case EDIT_LINE_POPUP:    
                dialog.setTitle("Edit Line");
                dialog.setHeaderText("Metro Line Details");
                dialog.setContentText("Name:");
                MetroLine line = (MetroLine)obj;
                dialog.getEditor().setText(line.toString());
                cp.setColor(line.getColor());
                break;
            case ADD_TEXT_POPUP:
                dialog.setTitle("Text Editor");
                dialog.setHeaderText("Text To Place");
                break;
        }
        return dialog;
    }
}
