/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.gui;

import djf.AppTemplate;
import djf.language.AppLanguageSettings;
import static djf.language.AppLanguageSettings.FILE_PROTOCOL;
import static djf.language.AppLanguageSettings.PATH_IMAGES;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static metro.css.metroStyle.CLASS_SPLASH;
import static metro.css.metroStyle.CLASS_SPLASH_LEFT;
import static metro.css.metroStyle.CLASS_SPLASH_MID;

/**
 *
 * @author Dan
 */
public class WelcomePane extends Stage{
    
    AppTemplate app;
    
    String selectedPath = null;
    boolean newFile = false;
    String workPath;
    
    List<File> fileNames;
    
    BorderPane root;
    VBox leftWindow;
    FlowPane mainWindow;
    
    Label workLabel;
    Hyperlink newMapLink;
    ImageView logo;
    
    public WelcomePane(AppTemplate app){
        this.app = app;
        
        workPath = AppLanguageSettings.PATH_WORK;
        loadFiles();
        initLayout();
        initControllers();
        initStyle();
    }
    
    private void initLayout(){
        
        root = new BorderPane();
        leftWindow = new VBox();
        mainWindow = new FlowPane(Orientation.VERTICAL);
        
        workLabel = new Label("Recent Work");
        newMapLink = new Hyperlink("Create New Metro Map");
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + "MetroLogo.png";
        logo = new ImageView(new Image(imagePath));
        
        root.setLeft(leftWindow);
        root.setCenter(mainWindow);
        
        mainWindow.getChildren().addAll(logo, newMapLink);
        
        leftWindow.getChildren().add(workLabel);
        addHyperLinks(leftWindow);
        
        
        Scene scene = new Scene(root, 900, 700);
        this.setTitle("Welcome to Metro Map Maker");
        this.setScene(scene);
    }
    
    private void loadFiles(){
        File folder = new File(workPath);
        fileNames = new ArrayList(Arrays.asList(folder.listFiles()));
    }
    
    private void addHyperLinks(Pane pane){
        fileNames.forEach(file ->{
            String path = file.getPath();
            //check if compatable file
            if(path.endsWith(".met")){
                
                int li = path.lastIndexOf("\\");
                String name = path.substring(li+1, path.length());
                name = name.replaceAll(".met$", "");
                
                //Make a new link and add it to the pane
                Hyperlink link = new Hyperlink(name);
                pane.getChildren().add(link);
                //
                link.setOnAction(e->{
                    selectedPath = path;
                    this.close();
                });
            }

        });
    }

    private void initControllers() {
        newMapLink.setOnAction(e ->{
           newFile = true;
           this.close();
        });
    }
    
    public boolean isNewFile(){
        return newFile;
    }
    
    public String getFilePath(){
        return selectedPath;
    }

    private void initStyle() {
        app.getGUI().applyStyleSheet(root);
        leftWindow.getStyleClass().add(CLASS_SPLASH_LEFT);
        mainWindow.getStyleClass().add(CLASS_SPLASH_MID);
        mainWindow.setAlignment(Pos.CENTER);
        root.getStyleClass().add(CLASS_SPLASH);
    }
}
