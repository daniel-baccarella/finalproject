/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;

/**
 *
 * @author Dan
 */
public class GridLayer extends Pane{
    
    private double strokeWidth;
    private double gridWidth;
    
    List<Line> verticalLines;
    List<Line> horizontalLines;
    
    public GridLayer(double gridWidth){
        this(gridWidth, .5d);
    }
    
    public GridLayer(double gridWidth, double strokeWidth){
        this.strokeWidth = strokeWidth;
        this.gridWidth = gridWidth;
        verticalLines = new ArrayList<>();
        horizontalLines = new ArrayList<>();
        addListeners();
    }
    
    private void addListeners(){
        this.widthProperty().addListener(e -> {
            updateVerticalLines();
        });
        this.heightProperty().addListener(e ->{
            updateHorizontalLines();
        });
        
    }
    
    private void updateVerticalLines(){
        
        this.getChildren().removeAll(verticalLines);
        verticalLines.clear();
        
        double paneWidth = this.widthProperty().get();
        for(double x = 0; x <= paneWidth; x += gridWidth){
            Line line = new Line();
            line.setStartX(x);
            line.setEndX(x);
            line.setStartY(0);
            line.endYProperty().bind(this.heightProperty());
            line.setStrokeWidth(strokeWidth);
            verticalLines.add(line);
            this.getChildren().add(line);
        }
    }
    
    private void updateHorizontalLines(){
        
        this.getChildren().removeAll(horizontalLines);
        horizontalLines.clear();
        
        double paneHeight = this.heightProperty().get();
        for(double x = 0; x <= paneHeight; x += gridWidth){
            Line line = new Line();
            line.setStartX(0);
            line.endXProperty().bind(this.widthProperty());
            line.setStartY(x);
            line.setEndY(x);
            line.setStrokeWidth(strokeWidth);
            horizontalLines.add(line);
            this.getChildren().add(line);
        }
    }

    /**
     *  Gets the double value of space between grid lines
     * @return the space between grid lines
     */
    public double getGridWidth() {
        return gridWidth;
    }
    
}
