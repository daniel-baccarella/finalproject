/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.gui;

import javafx.beans.Observable;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author Dan
 */
public class ColorPickerCircle extends StackPane{
    public Label colorText;
    public Circle circle;
    public ColorPicker colorPicker;
    
    public ColorPickerCircle(int radius){
        this(radius, true);
    }
    
    public ColorPickerCircle(int radius, boolean cpEnabled){
        colorText = new Label();
        circle = new Circle();
        colorPicker = new ColorPicker();
        
        //colorPicker.setShape(circle);
        
        this.getChildren().addAll(colorPicker, circle, colorText);
        
        colorPicker.setVisible(false);
        
        colorPicker.valueProperty().addListener((Observable e) ->{
            circle.setFill(colorPicker.getValue());
            colorText.setText(colorPicker.getValue().toString().substring(0, 8));
            colorText.setTextFill(colorPicker.getValue().invert());
            scaleText();
        });
        if(cpEnabled){
            circle.setOnMouseClicked(e ->{
                colorPicker.show();
            });

            colorText.setOnMouseClicked(e ->{
                colorPicker.show();
            });
        }
        circle.setRadius(radius);
        colorPicker.setValue(Color.BLACK);
        colorPicker.setValue(Color.WHITE);
    }
    
    public void scaleText(){
        this.layout();
        colorText.applyCss();
        if(circle.getRadius() == 0d || colorText.getWidth() == 0d){
            return;
        }
        double scale = (circle.getRadius()*1.60)/colorText.getWidth();
        colorText.setScaleX(scale);
        colorText.setScaleY(scale);
    }
    
    public void setRadius(double radius){
        circle.setRadius(radius);
    }
    
    public Color getColor(){
        return colorPicker.getValue();
    }
    
    public void setColor(Color color){
        colorPicker.setValue(color);
    }
}
