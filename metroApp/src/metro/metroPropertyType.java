package metro;

/**
 * This class provides the properties that are needed to be loaded for
 * setting up goLogoLo workspace controls including language-dependent
 * text.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public enum metroPropertyType {
    
    
    /*These are the icons for the metro app*/
    DISTANCE_ICON,
    DISTANCE_LARGE_ICON,
    EXPAND_ICON,
    LIST_ICON,
    ROTATE_ICON,
    SHRINK_ICON,
    ZOOM_IN_ICON,
    ZOOM_OUT_ICON,
    
    /*These are the tooltips for the metro app*/
    DISTANCE_TOOLTIP,
    EXPAND_TOOLTIP,
    LIST_TOOLTIP,
    ROTATE_TOOLTIP,
    SHRINK_TOOLTIP,
    ZOOM_IN_TOOLTIP,
    ZOOM_OUT_TOOLTIP,
    
    /* THESE ARE LABELS THAT WILL NEED LANGUAGE SPECIFIC CONTENT */
    BACKGROUND_COLOR,
    FILL_COLOR,
    OUTLINE_COLOR,
    OUTLINE_THICKNESS,
    BACKGROUND_COLOR_TEXT,
    FILL_COLOR_TEXT,
    OUTLINE_COLOR_TEXT,
    OUTLINE_THICKNESS_TEXT,

    /* goLogoLo WORKSPACE OPTIONS */
    FONT_FAMILY_COMBO_BOX_OPTIONS,
    FONT_SIZE_COMBO_BOX_OPTIONS,
    
    /* DEFAULT INITIAL POSITIONS WHEN ADDING IMAGES AND TEXT */
    DEFAULT_NODE_X,
    DEFAULT_NODE_Y
    
}