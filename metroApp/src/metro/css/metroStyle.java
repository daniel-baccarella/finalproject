package metro.css;

/**
 * This class lists all CSS style types for this application. These
 * are used by JavaFX to apply style properties to controls like
 * buttons, labels, and panes.

 * @author Daniel Baccarella
 * @author ?
 * @version 1.0
 */
public class metroStyle {
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS metro workspace's COMPONENTS TO A STYLE SHEET THAT IT USES
    public static final String CLASS_MAX_PANE = "max_pane";
    public static final String CLASS_RENDER_CANVAS = "render_canvas";
    public static final String CLASS_BUTTON = "button";
    public static final String CLASS_EDIT_TOOLBAR = "edit_toolbar";
    public static final String CLASS_EDIT_TOOLBAR_ROW = "edit_toolbar_row";
    public static final String CLASS_COLOR_CHOOSER_PANE = "color_chooser_pane";
    public static final String CLASS_COLOR_CHOOSER_CONTROL = "color_chooser_control";
    
    //Styling for toolbar class
    public static final String CLASS_TOOL_GROUP = "tool_group";
    public static final String CLASS_TOOL_VBOX = "tool_vbox";
    public static final String CLASS_TOOL_HBOX = "tool_hbox";
    public static final String CLASS_TOOL_CHILD = "tool_child";
    public static final String CLASS_HGROW_COMBO = "combo_hgrow";
    public static final String CLASS_TOOL_LABEL = "tool_label";
    
    //Style Classes for splash window
    public static final String CLASS_SPLASH_LEFT =  "left_splash_column";
    public static final String CLASS_SPLASH_MID =  "mid_splash_column";
    public static final String CLASS_SPLASH =  "splash_window";
    
    
    public static final String EMPTY_TEXT = "";
    public static final int BUTTON_TAG_WIDTH = 75;
}
