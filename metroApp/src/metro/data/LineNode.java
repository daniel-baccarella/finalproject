/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

import java.util.HashSet;
import java.util.Set;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Font;
import metro.exceptions.LineNotFoundException;

/**
 *
 * @author Dan
 */
public class LineNode extends Ellipse implements Copyable<LineNode>, Draggable{

    private LineNode() {
        //FOR USE IN COPY METHOD
    }

    /**
     *  A builder object for use when creating a new LineNode object
     */
    public static class LineNodeBuilder{
        String labelText;
        double radius;
        double strokeWidth;
        Color fillColor;
        Color strokeColor;
        MetroLine line;
        
        double centerX;
        double centerY;
        
        public LineNodeBuilder labelText(String name){
            labelText = name;
            return this;
        }
        public LineNodeBuilder radius(double r){
            radius = r;
            return this;
        }
        public LineNodeBuilder strokeWidth(double w){
            strokeWidth = w;
            return this;
        }
        public LineNodeBuilder fillColor(Color c){
            fillColor = c;
            return this;
        }
        public LineNodeBuilder strokeColor(Color c){
            strokeColor = c;
            return this;
        }
        public LineNodeBuilder line(MetroLine line){
            this.line = line;
            return this;
        }
        public LineNodeBuilder centerX(double centerX){
            this.centerX = centerX;
            return this;
        }
        public LineNodeBuilder centerY(double centerY){
            this.centerY = centerY;
            return this;
        }
        public LineNode buildEnd(){
            return new LineNode(this);
        }
        public Station buildStation(){
            return new Station(this);
        }
    }
    
    /**
     * The label for the node
     */
    public MetroLabel label;
    
    Set<MetroLine> lines;
    Set<Connection> connections;
    
    
    public LineNode(LineNodeBuilder builder){
        label = new MetroLabel(this, (int)builder.radius, -1);
        this.setCenterX(builder.centerX);
        this.setCenterY(builder.centerY);
        label.setX(this.getCenterX());
        label.setY(this.getCenterY());
        this.setRadiusX(builder.radius);
        this.setRadiusY(builder.radius);
        this.setStrokeWidth(builder.strokeWidth);
        this.setStroke(builder.strokeColor);
        this.setFill(builder.fillColor);
        this.setLabelText(builder.labelText);
        
        lines = new HashSet<>();
        connections = new HashSet<>();
        if(builder.line != null)
            lines.add(builder.line);
    }
    
    public static LineNodeBuilder getBuilder(){
        return new LineNodeBuilder();
    }
    
    
    public void setLabelText(String labelText) {
        label.setText(labelText);
    }
    
    public void moveLabel(){
        label.moveLabel();
    }
    
    @Override
    public LineNode copy() {
        LineNode copyNode       = new LineNode();
        copyNode.connections    = new HashSet<>(connections);
        copyNode.label          = this.label.copy();
        copyNode.lines       = new HashSet<>(lines);
        copyNode.setCenterX(this.getCenterX());
        copyNode.setCenterY(this.getCenterY());
        copyNode.setFill(this.getFill());
        copyNode.setRadiusX(this.getRadiusX());
        copyNode.setRadiusY(this.getRadiusY());
        
        return copyNode;
    }
    
    @Override
    public void restore(LineNode copyNode) {
        this.connections = copyNode.connections;
        this.label.restore(copyNode.label);
        this.lines = copyNode.lines;
        this.setCenterX(copyNode.getCenterX());
        this.setCenterY(copyNode.getCenterY());
        this.setFill(copyNode.getFill());
        this.setRadiusX(copyNode.getRadiusX());
        this.setRadiusY(copyNode.getRadiusY());
    }
    /**
     * Allows a translation with an x-y offset
     * @param x difference in x from last mouse keyframe
     * @param y difference in y from last mouse keyframe
     */
    @Override
    public void drag(int x, int y) {
        //Apply offset
        this.setCenterX(this.getCenterX() + x);
        this.setCenterY(this.getCenterY() + y);
    }
    
    @Override
    public String toString(){
        return label.getText();
    }
    
    /*
    public void positionLabel(){
        label.getParent().layout();
        double xOffset = label.getLayoutBounds().getWidth()/2;
        double yOffset = label.getLayoutBounds().getHeight()/2;
        label.setX(this.getCenterX()+xOffset);
        label.setY(this.getCenterY()+yOffset);  
    }
    */
        
    public void addLine(MetroLine line){
        lines.add(line);
    }
    
    public void removeLine(MetroLine line){
        lines.remove(line);
        for(Connection c: connections){
            if(c.getLine() == line){
                connections.remove(c);
            }
        }
    }
    
    public boolean isPartOfLine(MetroLine line){
        return lines.contains(line);
    }
    
     /**
     * Check if this Station is connected to another
     * @param station
     * @return 
     */
    public boolean isConnectedTo(LineNode station){
        try{
            for(Connection c: connections){
                if(c.getOtherNode(this) == station){
                    return true;
                }
            }
        }catch(LineNotFoundException e){
            e.printStackTrace();
        }
        return false;
    }

    public void removeConnection(Connection c) {
        connections.remove(c);
    }
    
    public void addConnection(Connection c){
        connections.add(c);
    }
    
    public void setText(String stationName) {
        label.setText(stationName);
    }
    
    public MetroLine getFirstLine(){
        return lines.iterator().next();
    }
    
    @Override
    public void snapToGrid(double gridWidth) {
        double x = this.getCenterX();
        double y = this.getCenterY();
        
        double closestX = Double.MAX_VALUE;
        double closestY = Double.MAX_VALUE;
        
        for(double xCor = 0d; true; xCor += gridWidth){
            double distance = Math.abs(xCor-x);
            if(distance > Math.abs(closestX-x)){
                break;
            }
            closestX = xCor;
        }
        
        for(double yCor = 0d; true; yCor += gridWidth){
            double distance = Math.abs(yCor-y);
            if(distance > Math.abs(closestY-y)){
                break;
            }
            closestY = yCor;
        }
        
        this.setCenterX(closestX);
        this.setCenterY(closestY);
    }
}
