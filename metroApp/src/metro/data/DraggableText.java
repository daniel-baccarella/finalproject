/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author Dan
 */
public class DraggableText extends Text implements Draggable, Copyable<DraggableText>{
    
    public String fontFamily;
    public int fontSize;
    public boolean isItalicized;
    public boolean isBolded;
    
    public DraggableText(int x, int y, String text) {
	setX(x);
	setY(y);
        setText(text);
    }

    private DraggableText() {
        //FOR USE IN COPY METHOD
    }
    
    @Override
    public void drag(int diffX, int diffY) {
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	xProperty().set(newX);
	yProperty().set(newY);
    }
    
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    public final Font getFontStyling(){
        Font styledFont = Font.font(fontFamily,
                            isBolded? FontWeight.BOLD : FontWeight.LIGHT,
                            isItalicized? FontPosture.ITALIC : FontPosture.REGULAR,
                            fontSize);
        return styledFont;
    }
    
    public void updateStyle(){
        this.setFont(getFontStyling());
    }

        @Override
    public void snapToGrid(double gridWidth) {
        double x = this.getX();
        double y = this.getY();
        
        double closestX = Double.MAX_VALUE;
        double closestY = Double.MAX_VALUE;
        
        for(double xCor = 0d; true; xCor += gridWidth){
            double distance = Math.abs(xCor-x);
            if(distance > Math.abs(closestX-x)){
                break;
            }
            closestX = xCor;
        }
        
        for(double yCor = 0d; true; yCor += gridWidth){
            double distance = Math.abs(yCor-y);
            if(distance > Math.abs(closestY-y)){
                break;
            }
            closestY = yCor;
        }
        
        this.setX(closestX);
        this.setY(closestY);
    }

    @Override
    public DraggableText copy() {
        DraggableText copyLabel = new DraggableText();
        
        copyLabel.setFill(this.getFill());
        copyLabel.setText(this.getText());
        
        copyLabel.setX(this.getX());
        copyLabel.setY(this.getY());
        
        copyLabel.fontFamily = this.fontFamily;
        copyLabel.fontSize = this.fontSize;
        copyLabel.isBolded = this.isBolded;
        copyLabel.isItalicized = this.isItalicized;
        
        copyLabel.updateStyle();
        
        return copyLabel;
    }

    @Override
    public void restore(DraggableText copyLabel) {    
        this.setFill(copyLabel.getFill());
        this.setText(copyLabel.getText());
        
        this.setX(copyLabel.getX());
        this.setY(copyLabel.getY());
        
        this.fontFamily = copyLabel.fontFamily;
        this.fontSize = copyLabel.fontSize;
        this.isBolded = copyLabel.isBolded;
        this.isItalicized = copyLabel.isItalicized;
        
        this.updateStyle();
    }
}
