/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

/**
 *
 * @author Dan
 */
public enum metroState {
    DRAGGING_ITEM,
    SELECTING_ITEM,
    PLACING_STATION,
    ADDING_STATION_TO_LINE,
    REMOVING_STATION_FROM_LINE,
    DRAGGING_NOTHING,
    PLACING_TEXT,
    PLACING_IMAGE
}
