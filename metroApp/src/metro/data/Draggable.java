/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

/**
 *
 * @author Dan
 */
public interface Draggable {
    public void drag(int x, int y);
    public void snapToGrid(double gridSize);
}
