/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import metro.exceptions.LineNotFoundException;

/**
 *
 * @author Dan
 */
public class Connection extends Line{
    
    public static class ConnectionBuilder{
        private final MetroLine metroLine;
        private final Color color;
        private boolean partOfLine;
        private double strokeWidth;
        private LineNode startConnection;
        private LineNode endConnection;
        
        public ConnectionBuilder(final Color color, final boolean partOfLine, MetroLine line){
            this.color = color;
            this.partOfLine = partOfLine;
            this.metroLine = line;
        }
        public ConnectionBuilder startConnection(final LineNode startConnection){
            this.startConnection = startConnection;
            return this;
        }
        public ConnectionBuilder endConnection(final LineNode endConnection){
            this.endConnection = endConnection;
            return this;
        }
        
        public ConnectionBuilder strokeThickness(double lineThickness) {
            this.strokeWidth = lineThickness;
            return this;
        }
        public Connection build(){
            return new Connection(this);
        }
    }
    LineNode startConnection;
    LineNode endConnection;
    
    private boolean partOfLine;
    private Color lineColor;
    
    private MetroLine metroLine;
    
    private Connection(ConnectionBuilder builder) {
        lineColor = builder.color;
        setStroke(builder.color);
        setPartOfLine(builder.partOfLine);
        if(builder.startConnection != null){
            setStart(builder.startConnection);
        }
        if(builder.endConnection != null){
            setEnd(builder.endConnection);
        }
        metroLine = builder.metroLine;
        this.setStrokeWidth((builder.strokeWidth == 0)?  3 : builder.strokeWidth);
    }
    
    public final void setPartOfLine(boolean partOfLine){
        this.partOfLine = partOfLine;
    }
    
    public boolean isPartOfLine(){
        return partOfLine;
    }
    
    /**
     * Sets the node at the start of the connection
     * @param node that is attached to start
     */
    public final void setStart(LineNode node){
        //Unbind the listeners so it doesnt leak
        if(startConnection != null){
            this.startXProperty().unbind();
            this.startYProperty().unbind();
        }
        //Change the node pointer
        this.startConnection = node;
        //Set up new bindings
        this.startXProperty().bind(startConnection.centerXProperty());
        this.startYProperty().bind(startConnection.centerYProperty());
    }
    /**
     * Sets the node at the end of the connection
     * @param node that is attached to end
     */
    public final void setEnd(LineNode node){
        //Unbind the listeners so it doesnt leak
        if(endConnection != null){
            this.endXProperty().unbind();
            this.endYProperty().unbind();
        }
        //Change the node pointer
        this.endConnection = node;
        //Set up new bindings
        this.endXProperty().bind(endConnection.centerXProperty());
        this.endYProperty().bind(endConnection.centerYProperty());
    }
    
    /**
     * This method returns the node opposite in the connection to the one passed as a parameter
     * @param node this is the node it will check against to find the other side of the connection
     * @return LineNode will return the node at the other end from the one passed as an arg
     * @throws LineNotFoundException 
     */
    public LineNode getOtherNode(LineNode node) throws LineNotFoundException{
        if(node == startConnection){
            return endConnection;
        }else if(node == endConnection){
            return startConnection;
        }
        throw new LineNotFoundException();
    }
  
    public void setColor(Color color){
        lineColor = color;
        this.setStroke(color);
    }
    
    public MetroLine getLine(){
        return metroLine;
    }
}
