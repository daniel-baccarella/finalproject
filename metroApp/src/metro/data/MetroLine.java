/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javax.json.JsonValue;
import metro.data.Connection.ConnectionBuilder;

/**
 *
 * @author Dan
 */
public class MetroLine implements Comparable<MetroLine>, Copyable<MetroLine>{
    
    List<Connection> connections;
    List<Station> stations;
    
    LineNode startNode;
    LineNode endNode;
    
    Color color;
    
    ObservableList childList;
    
    String lineName;
    
    double lineThickness;
    
    public MetroLine(String name, ObservableList childList){
        //Make connection with just a default black color
        this(name, Color.BLACK, childList);
    }
    
    public MetroLine(String name, Color color, ObservableList childList){
        
        this.color = color;
        this.lineName = name;
        
        connections = new ArrayList<>();
        stations = new LinkedList<>();
        
        //Enable start and end nodes
        startNode = LineNode.getBuilder()
                                .labelText(name)
                                .radius(20)
                                .fillColor(Color.TRANSPARENT)
                                .line(this)
                                .buildEnd();
        endNode = LineNode.getBuilder()
                                .labelText(name)
                                .radius(20)
                                .fillColor(Color.TRANSPARENT)
                                .line(this)
                                .buildEnd();
        Connection connection1 = new ConnectionBuilder(color, false, this)
                                        .startConnection(startNode)
                                        .endConnection(endNode)
                                        .strokeThickness(lineThickness)
                                        .build();
        
       connections.add(connection1);
       
       childList.addAll(startNode, endNode, startNode.label, endNode.label);
       this.childList = childList;
       redrawConnections();
    }

    private MetroLine() {
       //FOR USE IN COPY METHOD
    }
    /**
     * 
     * @param color of line and connections
     */
    public void setColor(Color color){
        this.color = color;
        for(Connection c: connections){
            c.setStroke(color);
        }
    }
    
    public boolean removeStation(Station station){
        boolean removed = stations.remove(station);
        redrawConnections();
        return removed;
    }
    
    public void addStation(Station station){
        
        station.addLine(this);
        
        double minDif = Double.MAX_VALUE;
        LineNode sNode = null;
        LineNode eNode = null;
        
        for(int i = 0; i < connections.size(); i++){
            Connection c = connections.get(i);
            LineNode n1 = c.startConnection;
            LineNode n2 = c.endConnection;
            
            double curDistance = getNodeDistance(n1, n2);
            
            double d1 = getNodeDistance(n1, station);
            double d2 = getNodeDistance(n2, station);
            double newDistance = d1 + d2;
            
            double dDif = newDistance - curDistance;
            
            if(dDif < minDif){
                minDif = dDif;
                sNode = n1;
                eNode = n2;
            }
        }
        int indexS = stations.indexOf(sNode);
        int indexE = stations.indexOf(eNode);
        if(indexS != -1 && indexE != -1){
            stations.add(indexS+1, station);
        }else if(indexS == -1){
            stations.add(0, station);
        }else{
            stations.add(stations.size(), station);
        }
        
        redrawConnections();
        
    }
    /**
     * Helper method for adding stations, to find distance between two stations
     * @param n1
     * @param n2
     * @return 
     */
    private double getNodeDistance(LineNode n1, LineNode n2){
        return getDistance(n1.getCenterX(), n2.getCenterX(), n1.getCenterY(), n2.getCenterY());
    }
    
    /**
     *  Helper method for add station, to find distance two x, y coordinates
     * @param x1
     * @param x2
     * @param y1
     * @param y2
     * @return double Pythagorean based distance between points
     */
    private double getDistance(double x1, double x2, double y1, double y2){
        double dx = x1 - x2;
        double dy = y1 - y2;
        double dxs = Math.pow(dx, 2d);
        double dys = Math.pow(dy, 2d);
        return Math.sqrt(dxs + dys);
    }
    /**
     * Removes all line elements from child list and disconnects all stations
     */
    public void disconnectLine(){
        for(Station s: stations){
            s.removeLine(this);
            s.removeLineConnections(this);
        }
        childList.removeAll(connections);
        childList.removeAll(startNode, endNode, startNode.label, endNode.label);
    }
    
    /**
     * Redraws all line connections and remakes all connections for the line
     */
    public final void redrawConnections(){
        //clear connections from gui
        childList.removeAll(connections);
        
        //clear connections from their stations
        for(Connection c: connections){
            LineNode start = c.startConnection;
            LineNode end = c.endConnection;
            
            //remove the old connections
            if(start instanceof Station) ((Station)start).removeConnection(c);
            if(end instanceof Station) ((Station)end).removeConnection(c);
        }
        //reset the connection list
        connections = new ArrayList<>();
        
        if(stations.size() > 0){
            Connection cs =  new ConnectionBuilder(color, false, this)
                                    .startConnection(startNode)
                                    .endConnection(stations.get(0))
                                    .build();
            
            connections.add(cs);
            childList.add(0, cs);
            
            //loop through stations and build connections between them
            for(int i = 0; i < stations.size()-1; i++){
                Station startStation = stations.get(i);
                Station endStation = stations.get(i+1);
                
                Connection cm =  new ConnectionBuilder(color, true, this)
                                    .startConnection(stations.get(i))
                                    .endConnection(stations.get(i+1))
                                    .build();
                
                connections.add(cm);
                childList.add(0, cm);
                
                //ADD NEW CONNECTIONS
                startStation.addConnection(cm);
                endStation.addConnection(cm);
            }
            
            Connection ce =  new ConnectionBuilder(color, false, this)
                                    .startConnection(stations.get(stations.size()-1))
                                    .endConnection(endNode)
                                    .build();
            
            connections.add(ce);
            childList.add(0, ce);
            
        }else{
            //Make the only connection between the start and end nodes
            Connection co =  new ConnectionBuilder(color, false, this)
                                    .startConnection(startNode)
                                    .endConnection(endNode)
                                    .build();
            
            connections.add(co);
            childList.add(0, co);
        }
        if(lineThickness != 0)
            setLineThickness(this.lineThickness);
    }
    /**
     * Allows user to set start node location
     * @param x int x pixel coordinate
     * @param y int y pixel coordinate
     */
    public void setStartLocation(int x, int y){
        startNode.setCenterX(x);
        startNode.setCenterY(y);
    }
    
    /**
     * Allows user to set end node location
     * @param x int x pixel coordinate
     * @param y int y pixel coordinate
     */
    public void setEndLocation(int x, int y){
        endNode.setCenterX(x);
        endNode.setCenterY(y);
    }
    
    public void setAllNewStations(Station...newStations){
        this.stations.clear();
        for(Station s: newStations){
            s.addLine(this);
            this.stations.add(s);
        }
        redrawConnections();
    }
    
    public void setName(String name){
        this.lineName = name;
        startNode.setText(name);
        endNode.setText(name);
    }
    public String getName(){
        return lineName;
    }
    public Color getColor(){
        return color;
    }
    @Override
    public String toString(){
        return lineName;
    }
    public List<Station> getStations(){
        return stations;
    }

    public double getStartLocationX() {
       return startNode.getCenterX();
    }

    public double getStartLocationY() {
        return startNode.getCenterY();
    }

    public double getEndLocationX() {
        return endNode.getCenterX();
    }

    public double getEndLocationY() {
        return endNode.getCenterY();
    }
    
    public Font getFont(){
        return startNode.label.getFont();
    }
    
    public void setFont(Font font){
        startNode.label.setFont(font);
        endNode.label.setFont(font);
    }
    
    public LineNode getStartNode(){
        return startNode;
    }
    
    public LineNode getEndNode(){
        return endNode;
    }
    
    public void setLineThickness(double thickness){
        this.lineThickness = thickness;
        for(Connection c: connections){
            c.setStrokeWidth(thickness);
        }
    }

    @Override
    public int compareTo(MetroLine line) {
        return this.startNode.label.getText().compareTo(line.getStartNode().label.getText());
    }

    public double getLineThickness() {
        return this.lineThickness;
    }

    @Override
    public MetroLine copy() {
        MetroLine copyLine      = new MetroLine();
        copyLine.connections    = new LinkedList(connections);
        copyLine.stations       = new LinkedList(stations);
        copyLine.color          = this.color;
        copyLine.startNode      = this.startNode.copy();
        copyLine.endNode        = this.endNode.copy();
        copyLine.color          = this.color;
        copyLine.lineThickness  = this.lineThickness;
        copyLine.lineName       = this.lineName;
        
        return copyLine;
    }

    @Override
    public void restore(MetroLine copyLine) {
        
        //Weve added or removed a station
        if(this.stations.size() != copyLine.stations.size()){
            //If we are removing a station
            if(this.stations.size() > copyLine.stations.size()){
                List<Station> tempList = new LinkedList<>(this.stations);
                tempList.removeAll(copyLine.stations);
                Station stationToRemove = tempList.get(0);
                this.removeStation(stationToRemove);
            }
            else{ //If we are adding a station
                List<Station> tempList = new LinkedList<>(copyLine.stations);
                tempList.removeAll(this.stations);
                Station stationToAdd = tempList.get(0);
                this.addStation(stationToAdd);
            }
        }
        
        this.startNode.restore(copyLine.startNode);
        this.endNode.restore(copyLine.endNode);
        this.setName(copyLine.lineName);
        this.setColor(copyLine.color);
        this.setLineThickness(copyLine.lineThickness);
        this.redrawConnections();
        
    }
}
