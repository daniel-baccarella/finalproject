/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Orientation;
import javafx.scene.text.Font;
import static javafx.scene.text.FontPosture.ITALIC;
import static javafx.scene.text.FontPosture.REGULAR;
import static javafx.scene.text.FontWeight.BOLD;
import static javafx.scene.text.FontWeight.NORMAL;
import javafx.scene.text.Text;
import javax.json.JsonValue;

/**
 *
 * @author Dan
 */
public class MetroLabel extends Text implements Copyable<MetroLabel>{
    
    private LineNode attachedNode;
    
    private ChangeListener<? super Number> xListener;
    private ChangeListener<? super Number> yListener;
    
    int position;
    
    double xOffset;
    double yOffset;
    
    double offsetRadius;
    
    //Font parameters
    String fontFamily;
    double fontSize;
    boolean isBold;
    boolean isItalic;
    
    //Rotation enum
    Orientation orientation;
    
    public MetroLabel(LineNode node, double offsetRadius){
        this(node, offsetRadius, 0);
    }
    
    MetroLabel(LineNode node, double offsetRadius, int position){
        super();
        
        attachedNode = node;
        
        this.orientation = Orientation.HORIZONTAL;
        
        this.offsetRadius = offsetRadius;
        //Set position to starting poisition
        initializeListeners();
        if(position != -1){
            initializeListeners();
            bindListeners();
            setPosition(position);
        }else{
            this.position = -1;
            xOffset = 0;//this.getBoundsInLocal().getWidth()/2d;
            yOffset = this.getBoundsInLocal().getHeight()/4d;
            initializeListeners();
            bindListeners();
        }
        
        //Font initialization
        fontFamily = Font.getDefault().getFamily();
        fontSize = Font.getDefault().getSize();
        isBold = false;
        isItalic = false;
    }

    private MetroLabel() {
        //FOR USE IN COPY METHOD
    }
    
    private void initializeListeners(){
        
        xListener = (ov, t, tt) ->{
            fireTranslate();
        };
        yListener = (ov, t, tt) ->{
            fireTranslate();
        };
    }
    
    private void bindListeners(){
        attachedNode.centerXProperty().addListener(xListener);
        attachedNode.centerYProperty().addListener(yListener);
    }
    
    public void fireTranslate(){
        this.setX(attachedNode.getCenterX() + xOffset);
        this.setY(attachedNode.getCenterY() + yOffset);
    }
    
    public void unbindListeners(){
        attachedNode.centerXProperty().removeListener(xListener);
        attachedNode.centerYProperty().removeListener(yListener);
    }
    
    public void moveLabel(){
        switch(position){
            case 0:
            case 1:
            case 2: setPosition(position+1);
                    break;
            case 3: setPosition(0);
        }
    }
    private void setPosition(int pos){
        switch(pos){
            case 0: xOffset = offsetRadius;
                    yOffset = -offsetRadius;
                    position = 0;
                    break;
            case 1: xOffset = offsetRadius;
                    yOffset = offsetRadius;
                    position = 1;
                    break;
            case 2: xOffset = -offsetRadius - (int)this.getBoundsInLocal().getWidth();
                    yOffset = -offsetRadius;
                    position = 2;
                    break;
            case 3: xOffset = -offsetRadius - (int)this.getBoundsInLocal().getWidth();
                    yOffset = offsetRadius;
                    position = 3;
                    break;
        }
        fireTranslate();
    }
    
    public void setOffsetRadius(double radius){
        this.offsetRadius = radius;
        this.setPosition(position);
    }

    @Override
    public MetroLabel copy() {
        MetroLabel copyLabel = new MetroLabel();
        copyLabel.attachedNode = this.attachedNode;
        copyLabel.offsetRadius = this.offsetRadius;
        copyLabel.position = this.position;
        
        copyLabel.orientation = this.orientation;
        
        copyLabel.setFill(this.getFill());
        copyLabel.setText(this.getText());
        
        copyLabel.fontFamily = this.fontFamily;
        copyLabel.fontSize = this.fontSize;
        copyLabel.isBold = this.isBold;
        copyLabel.isItalic = this.isItalic;
        
        return copyLabel;
    }

    @Override
    public void restore(MetroLabel copyLabel) {
        this.offsetRadius = copyLabel.offsetRadius;
        
        if(copyLabel.position == -1){
            xOffset = 0;//this.getBoundsInLocal().getWidth()/2d;
            yOffset = this.getBoundsInLocal().getHeight()/4d;
            initializeListeners();
            bindListeners();
        }
        else{
            this.setPosition(copyLabel.position);
        }
        
        this.setText(copyLabel.getText());
        this.setFill(copyLabel.getFill());
        
        this.setOrientation(copyLabel.orientation);
        
        this.fontFamily     = copyLabel.fontFamily;
        this.fontSize       = copyLabel.fontSize;
        this.isBold         = copyLabel.isBold;
        this.isItalic       = copyLabel.isItalic;
        updateFont();
    }
    
    private void updateFont(){
        Font font = Font.font(fontFamily,
                             (isBold)? BOLD : NORMAL,
                             (isItalic)? ITALIC : REGULAR,
                              fontSize);
        this.setFont(font);
    }
    
    public void setBold(boolean bold){
        this.isBold = bold;
        updateFont();
    }
    
    public void setItalic(boolean italic){
        this.isItalic = italic;
        updateFont();
    }
    
    public void setFontFamily(String fontFamily){
        this.fontFamily =  fontFamily;
        updateFont();
    }
    
    public void setFontSize(double fontSize){
        this.fontSize = fontSize;
        updateFont();
    }
    
    public void toggleOrientation(){
        if(this.orientation == Orientation.HORIZONTAL){
            setOrientation(Orientation.VERTICAL);
        }else if(this.orientation == Orientation.VERTICAL){
            setOrientation(Orientation.HORIZONTAL);
        }
    }
    
    public void setOrientation(Orientation orientation){
        
        this.orientation = orientation;
        
        if(orientation == Orientation.HORIZONTAL){
            this.setRotate(0);
        }else if(orientation == Orientation.VERTICAL){
            this.setRotate(90);
        }
    }
    
    public boolean isBold(){
        return isBold;
    } 
    public boolean isItalic(){
        return isItalic;
    }
    public String getFontFamily(){
        return fontFamily;
    }
    public double getFontSize(){
        return fontSize;
    }

    public Orientation getOrientation() {
        return this.orientation;
    }


    
}
