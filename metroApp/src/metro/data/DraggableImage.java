/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;
import java.io.File;
import java.net.MalformedURLException;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Dan
 */
public class DraggableImage extends Rectangle implements Draggable, Copyable<DraggableImage>{
    
    String filePath;
    
    public DraggableImage(double x, double y){
        this.setX(x);
        this.setY(y);
    }

    private DraggableImage() {
        //FOR USE IN COPY METHOD
    }

    @Override
    public void drag(int diffX, int diffY) {
        double newX = getX() + diffX;
	double newY = getY() + diffY;
	xProperty().set(newX);
	yProperty().set(newY);
    }
    
    public void setImage(String filePath) throws MalformedURLException{
        File newFile = new File(filePath);
        this.filePath =  newFile.toURI().toURL().toExternalForm();
        Image img = new Image(this.filePath);
        this.setHeight(img.getHeight());
        this.setWidth(img.getWidth());
        setFill(new ImagePattern(img));
    }
    
    public String getFilePath(){
        return filePath;
    }
    
    @Override
    public void snapToGrid(double gridWidth) {
        double x = this.getX();
        double y = this.getY();
        
        double closestX = Double.MAX_VALUE;
        double closestY = Double.MAX_VALUE;
        
        for(double xCor = 0d; true; xCor += gridWidth){
            double distance = Math.abs(xCor-x);
            if(distance > Math.abs(closestX-x)){
                break;
            }
            closestX = xCor;
        }
        
        for(double yCor = 0d; true; yCor += gridWidth){
            double distance = Math.abs(yCor-y);
            if(distance > Math.abs(closestY-y)){
                break;
            }
            closestY = yCor;
        }
        
        this.setX(closestX);
        this.setY(closestY);
    }

    @Override
    public DraggableImage copy() {
        DraggableImage imageCopy =  new DraggableImage();
        imageCopy.setFill(this.getFill());
        imageCopy.setX(this.getX());
        imageCopy.setY(this.getY());
        imageCopy.filePath = this.filePath;
        
        return imageCopy;
    }

    @Override
    public void restore(DraggableImage imageCopy) {

        this.setFill(imageCopy.getFill());
        this.setX(imageCopy.getX());
        this.setY(imageCopy.getY());
        this.filePath = imageCopy.filePath;
    }
}
