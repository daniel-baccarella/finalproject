/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

import djf.components.AppDataComponent;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import metro.data.LineNode.LineNodeBuilder;
import static metro.data.metroState.SELECTING_ITEM;
import metro.exceptions.LineNotFoundException;
import metro.gui.ColorPickerCircle;
import metro.gui.GridLayer;
import metro.gui.PopupFactory;
import static metro.gui.PopupFactory.Popup.ADD_TEXT_POPUP;
import metro.gui.metroWorkspace;
import metro.metroApp;
import metro.transactions.PaneTransaction;
import metro.transactions.RestorableTransaction;

/**
 *
 * @author Dan
 */
public class metroData implements AppDataComponent{
    
    PopupFactory popupFactory = new PopupFactory();
    
    //Gui Elements
    metroApp app;
    Pane canvas;
    Pane backgroundLayer;
    GridLayer gridLayer;
    ObservableList<Node> paneItems; 
    
    //Data Elements
    Set<MetroLine> metroLines;
    Set<Station> stations;
    Set<DraggableImage> images;
    Set<DraggableText> texts;
    
    //State and selection
    metroState state;
    Node selectedItem;
    
    // USE THIS WHEN THE SHAPE IS SELECTED
    Effect highlightedEffect;
    
    //BACKGROUND COLOR FOR PERSISTANCE
    Color backgroundColor;
    
    jTPS undoStack;
    long totalTransactions = 0;

    public metroData(metroApp app) {
        this.app =  (metroApp)app;
        setState(SELECTING_ITEM);
        
        metroLines = new HashSet<>();
        stations = new HashSet<>();
        images = new HashSet<>();
        texts = new HashSet<>();
        
        // THIS IS FOR THE SELECTED SHAPE
	DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(.5);
	dropShadowEffect.setColor(Color.YELLOW);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(15);
	highlightedEffect = dropShadowEffect;
        
        undoStack = app.getTPS();
    }
    
    public void setCanvas(Pane canvas, Pane backgroundLayer, GridLayer gridLayer){
        this.canvas = canvas;
        this.gridLayer = gridLayer;
        this.backgroundLayer = backgroundLayer;
        paneItems = canvas.getChildren();
        setBackgroundColor(Color.ALICEBLUE);
    }
    
    
    
    public void addLine(String name, Color color){
        MetroLine line = new MetroLine(name, color, paneItems);
        line.setStartLocation(10, 10);
        line.setEndLocation(30, 30);
        metroLines.add(line);
        
        setSelectedItem(line.startNode);
        
        //update workspace
        app.getWorkspaceComponent().reloadWorkspace(this);
        markEdited();
    }
    
    public void addStationToLine(Station station, MetroLine line){
        if(!line.stations.contains(station)){
            RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
            line.addStation(station);
            trans.setAfter(line);
            markEdited();
            addTransaction(trans);
        }
    }
    
    public void removeStationFromLine(Station station, MetroLine line){
        RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
        line.removeStation(station);
        trans.setAfter(line);
        markEdited();
        addTransaction(trans);
    }
    
    public void addStation(int x, int y) {
        
        ColorPickerCircle cp = new ColorPickerCircle(20);
        Color otherColor = ((metroWorkspace)app.getWorkspaceComponent()).stationColorPicker.getColor();
        cp.setColor(otherColor);
        
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add Station");
        dialog.setHeaderText("Metro Station Details");
        dialog.setContentText("Name:");
        dialog.setGraphic(cp);
        Optional<String> result = dialog.showAndWait();
        
        String lineName = "";
        if (result.isPresent() && !result.get().equals("")){
            lineName = result.get();
            Station station = new LineNodeBuilder()
                .fillColor(cp.getColor())
                .labelText(lineName)
                .radius(15)
                .centerX(x)
                .centerY(y)
                .buildStation();
       
            stations.add(station);
            paneItems.addAll(station, station.label);
            //setSelectedItem(station);
        }
        
        //update workspace
        app.getWorkspaceComponent().reloadWorkspace(this);
        markEdited();
    }
    
    public void addImage(int x, int y){
        FileChooser imageChooser = new FileChooser();
        imageChooser.setTitle("Pick an image");
        File imageFile = imageChooser.showOpenDialog(app.getGUI().getWindow());
        if(imageFile != null){
            DraggableImage imageNode = new DraggableImage(x, y);
            try{
                imageNode.setImage(imageFile.toString());
                canvas.getChildren().add(imageNode);
                images.add(imageNode);
                markEdited();
            }catch(MalformedURLException e){
                e.printStackTrace();
            }
        }
    }
    
    public void removeImage(DraggableImage image){
        if(image != null){
            PaneTransaction trans = new PaneTransaction(canvas);
            canvas.getChildren().remove(image);
            images.remove(image);
            trans.setAfter(canvas);
            markEdited();
            addTransaction(trans);
        }
    }
    
    public void addText(int x, int y){
        
        metroWorkspace workspace = (metroWorkspace)app.getWorkspaceComponent();
        
        TextInputDialog textPopup = popupFactory.getPopup(ADD_TEXT_POPUP);
        Optional<String> textOpt = textPopup.showAndWait();
        if(textOpt.isPresent() && !textOpt.get().equals("")){
            
            PaneTransaction trans = new PaneTransaction(canvas);
            
            DraggableText textNode = new DraggableText(x, y, textOpt.get());
            textNode.fontFamily = workspace.fontFamilyComboBox.getValue();
            textNode.fontSize = workspace.fontSizeComboBox.getValue();
            textNode.isBolded = workspace.boldToggle.isSelected();
            textNode.isItalicized = workspace.italicToggle.isSelected();
            textNode.updateStyle();
            canvas.getChildren().add(textNode);
            texts.add(textNode);
            
            trans.setAfter(canvas);
            
            markEdited();
            addTransaction(trans);
        }
    }
    
    public void removeText(DraggableText textNode){
        if(textNode != null){
            PaneTransaction trans = new PaneTransaction(canvas);
            canvas.getChildren().remove(textNode);
            texts.remove(textNode);
            trans.setAfter(canvas);
            markEdited();
            addTransaction(trans);
        }
    }
    
    public void changeLineColor(MetroLine line, Color color){
        RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
        line.setColor(color);
        trans.setAfter(line);
        resetData();
        markEdited();
        addTransaction(trans);
    }
    
    public void changeLineName(MetroLine line, String name){
        RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
        line.setName(name);
        resetData();
        trans.setAfter(line);
        markEdited();
        addTransaction(trans);
    }
    
    public void changeStationName(Station station, String stationName) {
        RestorableTransaction<LineNode> trans = new RestorableTransaction<>(station);
        station.setText(stationName);
        trans.setAfter(station);
        markEdited();
        addTransaction(trans);
    }

    public void changeStationColor(Station station, Color color) {
        RestorableTransaction<LineNode> trans = new RestorableTransaction<>(station);
        station.setFill(color);
        trans.setAfter(station);
        markEdited();
        addTransaction(trans);
    }
    
    public metroState getState() {
	return state;
    }

    public final void setState(metroState state) {
	this.state = state;
    }

    public boolean isInState(metroState testState) {
	return state == testState;
    }
    
    public void toggleLabelOrientation(LineNode ln){
        if(ln != null){
            RestorableTransaction<MetroLabel> trans = new RestorableTransaction<>(ln.label);
            ln.label.toggleOrientation();
            trans.setAfter(ln.label);
            markEdited();
            addTransaction(trans);
        }
    }
    
    public void moveLabel(LineNode ln){
        if(ln != null){
            ln.moveLabel();
        }
    }
    
    public Node selectTopItem(int x, int y) {
        Shape shape = (Shape)getTopItem(x, y);
	if (shape == selectedItem)
	    return shape;
	
	if (selectedItem != null) {
	    unhighlightSelected();
	}
        
        selectedItem = shape;
        
	if (shape != null) {
	    highlightShape(shape);
	    metroWorkspace workspace = (metroWorkspace)app.getWorkspaceComponent();
	    workspace.loadSelectedItemSettings(shape, this);
	}
        
	if (shape != null) {
	    //((Draggable)shape).start(x, y);
	}
	return shape;
    }
    
    public Node getTopItem(int x, int y) {
        
	for (int i = paneItems.size() - 1; i >= 0; i--) {
	    Node node = paneItems.get(i);
	    if (node.contains(x, y) && !filteredItem(node)) {
		return node;
	    }
	}
	return null;
    }
    /**
     * Helper method for getTopItem, to filter out illegitimate items
     * @param node to check if it should be filtered
     * @return true if item is banned, false if not
     */
    private boolean filteredItem(Object node){
        if (node instanceof MetroLabel) return true;
        return false;
    }

    
    public Node getSelectedItem(){
        return selectedItem;
    }
    
    public void unhighlightSelected() {
	selectedItem.setEffect(null);
    }
    
    public void setSelectedItem(Shape shape){
        if (shape == selectedItem)
	    return;
	
	if (selectedItem != null) {
	    unhighlightSelected();
	}
        
        selectedItem = shape;
        
	if (shape != null) {
	    highlightShape(shape);
	    metroWorkspace workspace = (metroWorkspace)app.getWorkspaceComponent();
	    workspace.loadSelectedItemSettings(shape, this);
	}
    }
    
    public void highlightShape(Shape shape) {
	shape.setEffect(highlightedEffect);
        //enable cut & copy on starting new highlight
        metroWorkspace workspace = (metroWorkspace) app.getWorkspaceComponent();
        //workspace.setCopyDisable(false);
        //workspace.setCutDisable(false);
    }
    
    public Set<MetroLine> getMetroLines(){
        return metroLines;
    }
    
    public Set<Station> getStations(){
        return stations;
    }

    @Override
    public void resetData() {
        metroWorkspace workspace = (metroWorkspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace(this);
    }

    public void setBackgroundColor(Color color){
        PaneTransaction trans = new PaneTransaction(backgroundLayer);
        backgroundColor = color;
        backgroundLayer.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        trans.setAfter(backgroundLayer);
        markEdited();
        addTransaction(trans);
    }

    public void removeLine(MetroLine line) {
        line.disconnectLine();
        metroLines.remove(line);
        resetData();
        markEdited();
    }
    
    public void removeStation(Station station){
        station.disconnectStation();
        stations.remove(station);
        paneItems.removeAll(station, station.label);
        resetData();
        markEdited();
    }
    
    public Color getBackgroundColor(){
        return backgroundColor;
    }
    
    public ObservableList<Node> getChildList(){
        return paneItems;
    }
    
    public Pane getCanvas(){
        return canvas;
    }
    
    public void addAndResetAllData(Set<Station> stations, Set<MetroLine> lines){
        //paneItems.clear();
        this.stations.clear();
        this.metroLines.clear();
        this.metroLines = lines;
        this.stations = stations;
        for(Station s: stations){
            paneItems.addAll(s, s.label);
        }
        canvas.layout();
        resetData();
    }

    public void setLineThickness(double lineThickness, MetroLine line) {
        line.setLineThickness(lineThickness);
        markEdited();
    }

    public void setStationSize(double stationRadius, Station station) {
        station.setRadius(stationRadius);
        markEdited();
    }
    
    public List<LineNode> getRoute(Station fromStation, Station toStation){
        Set<LineNode> visited = new HashSet<>();
        visited.add(fromStation);
        List<LineNode> stations = findRouteFrom(fromStation, toStation, fromStation, new HashSet<LineNode>());
        if(stations != null){
            Collections.reverse(stations);
        }
        return stations;
    }
    
    private List<LineNode> findRouteFrom(LineNode from, LineNode to, LineNode cur, HashSet<LineNode> previousStations){
        
        System.out.println("Station: " + cur);
        
        List<List<LineNode>> possibleRoutesFromThis = new LinkedList<>();
        
        for(Connection c: cur.connections){
            if(c.isPartOfLine()){
                try {
                    LineNode nextStation = c.getOtherNode(cur);
                    
                    if(nextStation == to){
                        List<LineNode> connectionList = new LinkedList<>();
                        connectionList.add(to);
                        connectionList.add(cur);
                        return connectionList;
                    }
                    
                    List<LineNode> possibleRoutesFromNext = null;
                    
                    if(!previousStations.contains(nextStation) && nextStation != from){
                        HashSet<LineNode> newPrevStations = new HashSet<>(previousStations);
                        newPrevStations.add(cur);
                        possibleRoutesFromNext = findRouteFrom(from, to, nextStation, newPrevStations);
                    }
                    
                    if(possibleRoutesFromNext != null){
                        possibleRoutesFromThis.add(possibleRoutesFromNext);
                    }
                    
                } catch (LineNotFoundException ex) {
                    Logger.getLogger(metroData.class.getName()).log(Level.SEVERE, null, ex);
                }
            }            
        }
        
        if(!possibleRoutesFromThis.isEmpty()){
            List<LineNode> shortestRoute = possibleRoutesFromThis.stream()
                                                                 .min((list1, list2) ->{
                                                                    return Integer.compare(list1.size(), list2.size());
                                                                 }).get();
            shortestRoute.add(cur);
            return shortestRoute;
        }else{
             return null;
        }
        
    }
    
    public void markEdited(){
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void snapStationToGrid(Station station) {
        RestorableTransaction<LineNode> trans = new RestorableTransaction<>(station);
        station.snapToGrid(gridLayer.getGridWidth());
        trans.setAfter(station);
        markEdited();
        addTransaction(trans);
    }
    
    public void addTransaction(jTPS_Transaction transaction){
        
        totalTransactions++;
        
        if(totalTransactions < 3){
            return;
        }
        undoStack.addTransaction(transaction);
        app.getGUI().updateToolbarControls(true);
    }
    
    /******************************************************************
     * 
     *      FONT EDIT METHODS
     * 
     *******************************************************************/
    
    //COLOR METHODS

    public void changeMetroLabelFontColor(MetroLabel metLabel, Color color) {
        RestorableTransaction<MetroLabel> trans = new RestorableTransaction<>(metLabel);
        metLabel.setFill(color);
        trans.setAfter(metLabel);
        markEdited();
        addTransaction(trans);
    }

    public void changeLineLabelFontColor(LineNode endNode, Color color) {
        
        MetroLine line = endNode.lines.stream().findFirst().get();
        
        RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
        
        MetroLabel startLabel = line.startNode.label;
        MetroLabel endLabel = line.endNode.label;
        startLabel.setFill(color);
        endLabel.setFill(color);
        
        trans.setAfter(line);
        markEdited();
        addTransaction(trans);
    }

    public void changeDraggableTextColor(DraggableText text, Color color) {
        RestorableTransaction<DraggableText> trans = new RestorableTransaction<>(text);
        text.setFill(color);
        trans.setAfter(text);
        markEdited();
        addTransaction(trans);
    }

    
    //  SIZE METHODS
    
    public void changeMetroLabelFontSize(MetroLabel metLabel, double size) {
        RestorableTransaction<MetroLabel> trans = new RestorableTransaction<>(metLabel);
        metLabel.setFontSize(size);
        trans.setAfter(metLabel);
        markEdited();
        addTransaction(trans);
    }

    public void changeLineLabelFontSize(LineNode endNode, double size) {
               
        MetroLine line = endNode.lines.stream().findFirst().get();
        
        RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
        
        MetroLabel startLabel = line.startNode.label;
        MetroLabel endLabel = line.endNode.label;
        startLabel.setFontSize(size);
        endLabel.setFontSize(size);
        
        trans.setAfter(line);
        markEdited();
        addTransaction(trans);
    }

    public void changeDraggableTextSize(DraggableText text, double size) {
        RestorableTransaction<DraggableText> trans = new RestorableTransaction<>(text);
        text.fontSize = (int)size;
        text.updateStyle();
        trans.setAfter(text);
        markEdited();
        addTransaction(trans);
    }

    
    // FAMILY METHODS
    
    public void changeMetroLabelFontFamily(MetroLabel metLabel, String fontFamily) {
        RestorableTransaction<MetroLabel> trans = new RestorableTransaction<>(metLabel);
        metLabel.setFontFamily(fontFamily);
        trans.setAfter(metLabel);
        markEdited();
        addTransaction(trans);
    }

    public void changeLineLabelFontFamily(LineNode endNode, String fontFamily) {
                       
        MetroLine line = endNode.lines.stream().findFirst().get();
        
        RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
        
        MetroLabel startLabel = line.startNode.label;
        MetroLabel endLabel = line.endNode.label;
        startLabel.setFontFamily(fontFamily);
        endLabel.setFontFamily(fontFamily);
        
        trans.setAfter(line);
        markEdited();
        addTransaction(trans);
    }

    public void changeDraggableTextFamily(DraggableText text, String fontFamily) {
        RestorableTransaction<DraggableText> trans = new RestorableTransaction<>(text);
        text.fontFamily = fontFamily;
        text.updateStyle();
        trans.setAfter(text);
        markEdited();
        addTransaction(trans);
    }

    
    // BOLD METHODS
    
    public void changeMetroLabelFontBold(MetroLabel metLabel, boolean bold) {
        RestorableTransaction<MetroLabel> trans = new RestorableTransaction<>(metLabel);
        metLabel.setBold(bold);
        trans.setAfter(metLabel);
        markEdited();
        addTransaction(trans);
    }

    public void changeLineLabelFontBold(LineNode endNode, boolean bold) {
                       
        MetroLine line = endNode.lines.stream().findFirst().get();
        
        RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
        
        MetroLabel startLabel = line.startNode.label;
        MetroLabel endLabel = line.endNode.label;
        startLabel.setBold(bold);
        endLabel.setBold(bold);
        
        trans.setAfter(line);
        markEdited();
        addTransaction(trans);
    }

    public void changeDraggableTextBold(DraggableText text, boolean bold) {
        RestorableTransaction<DraggableText> trans = new RestorableTransaction<>(text);
        text.isBolded = bold;
        text.updateStyle();
        trans.setAfter(text);
        markEdited();
        addTransaction(trans);
    }
    
    
    //ITALIC METHODS

    public void changeMetroLabelFontItalic(MetroLabel metLabel, boolean italic) {
        RestorableTransaction<MetroLabel> trans = new RestorableTransaction<>(metLabel);
        metLabel.setItalic(italic);
        trans.setAfter(metLabel);
        markEdited();
        addTransaction(trans);
    }

    public void changeLineLabelFontItalic(LineNode endNode, boolean italic) {
                       
        MetroLine line = endNode.lines.stream().findFirst().get();
        
        RestorableTransaction<MetroLine> trans = new RestorableTransaction<>(line);
        
        MetroLabel startLabel = line.startNode.label;
        MetroLabel endLabel = line.endNode.label;
        startLabel.setItalic(italic);
        endLabel.setItalic(italic);
        
        trans.setAfter(line);
        markEdited();
        addTransaction(trans);
    }

    public void changeDraggableTextItalic(DraggableText text, boolean italic) {
        RestorableTransaction<DraggableText> trans = new RestorableTransaction<>(text);
        text.isItalicized = italic;
        text.updateStyle();
        trans.setAfter(text);
        markEdited();
        addTransaction(trans);
    }

    public void setBackgroundImage() {
        FileChooser imageChooser = new FileChooser();
        imageChooser.setTitle("Pick an image");
        File imageFile = imageChooser.showOpenDialog(app.getGUI().getWindow());
        if(imageFile != null){
            try{
            String filePath =  imageFile.toURI().toURL().toExternalForm();
            Image img = new Image(filePath);
            
            BackgroundImage myBI= new BackgroundImage(img,
                                        BackgroundRepeat.REPEAT, 
                                        BackgroundRepeat.REPEAT, 
                                        BackgroundPosition.CENTER,
                                        BackgroundSize.DEFAULT);
            
            backgroundLayer.setBackground(new Background(myBI));
            
            }catch(MalformedURLException e){
                e.printStackTrace();
            }
        }
    }

}
