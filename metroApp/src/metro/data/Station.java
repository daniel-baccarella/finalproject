/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.data;

import java.util.LinkedList;
import java.util.List;
import javafx.scene.paint.Color;

/**
 *
 * @author Dan
 */
public class Station extends LineNode implements Comparable<Station>{

    public Station(LineNodeBuilder builder){
        super(builder);
        label = new MetroLabel(this, (int)builder.radius);
        label.setText(builder.labelText);
        this.setStroke(Color.BLACK);
        this.setStrokeWidth(2);
    }
    /**
     * Removes all connections going through this station from the specified line
     * @param line to remove itself from
     */
    public void removeLineConnections(MetroLine line) {
        List<Connection> toRemove =  new LinkedList<>();
        for(Connection c: connections){
            if(c.getLine() == line){
                toRemove.add(c);
            }
        }
        connections.removeAll(toRemove);
    }
    /**
     * Removes this station from all lines it is a part of using their removeStation() function
     */
    public void disconnectStation(){
        for(MetroLine line: lines){
            line.removeStation(this);
        }
    }
    
    public void addLine(MetroLine line){
        lines.add(line);
    }

    @Override
    public int compareTo(Station s) {
        return label.getText().compareTo(s.label.getText());
    }

    void setRadius(double stationRadius) {
        this.setRadiusX(stationRadius);
        this.setRadiusY(stationRadius);
        label.setOffsetRadius(stationRadius);
    }
}
