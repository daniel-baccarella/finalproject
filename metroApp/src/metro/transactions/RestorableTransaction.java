/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.transactions;

import jtps.jTPS_Transaction;
import metro.data.Copyable;
import metro.data.Station;

/**
 *
 * @author Dan
 */
public class RestorableTransaction <C extends Copyable<C>> implements jTPS_Transaction{
    
    C beforeState;
    C afterState;
    final C ogObjPointer; //Original object pointer
    
    public RestorableTransaction(C original){
        this.ogObjPointer = original;
        beforeState = original.copy();
    }

    @Override
    public void doTransaction() {
        ogObjPointer.restore(afterState);
    }

    @Override
    public void undoTransaction() {
        ogObjPointer.restore(beforeState);
    }

    public void setAfter(C og_after) {
       afterState = og_after.copy();
    }
    
}
