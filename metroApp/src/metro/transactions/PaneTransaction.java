/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro.transactions;

import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author Dan
 */
public class PaneTransaction implements jTPS_Transaction{
    Background befBackground;
    Background aftBackground;
    
    List<Node> befChildList;
    List<Node> aftChildList;
    
    double befWidth;
    double aftWidth;
    double befHeight;
    double aftHeight;
    
    final Pane ogObjPointer; //Original object pointer
    
    public PaneTransaction(Pane original){
        this.ogObjPointer = original;
        befBackground = original.getBackground();
        befChildList = Arrays.asList(original.getChildren().toArray(new Node[0]));
        befWidth = original.getWidth();
        befHeight = original.getHeight();
    }
    
    public void setAfter(Pane og_after){
        aftBackground = og_after.getBackground();
        aftChildList = Arrays.asList(og_after.getChildren().toArray(new Node[0]));
        aftWidth = og_after.getWidth();
        aftHeight = og_after.getHeight();
    }

    @Override
    public void doTransaction() {
        ogObjPointer.setBackground(aftBackground);
        ogObjPointer.getChildren().clear();
        ogObjPointer.getChildren().addAll(aftChildList);
        
        ogObjPointer.setMinWidth(aftWidth);
        ogObjPointer.setMaxWidth(aftWidth);
        ogObjPointer.setMinHeight(aftHeight);
        ogObjPointer.setMaxHeight(aftHeight);
        
        ogObjPointer.getParent().layout();
    }

    @Override
    public void undoTransaction() {
        ogObjPointer.setBackground(befBackground);
        ogObjPointer.getChildren().clear();
        ogObjPointer.getChildren().addAll(befChildList);
        
        ogObjPointer.setMinWidth(befWidth);
        ogObjPointer.setMaxWidth(befWidth);
        ogObjPointer.setMinHeight(befHeight);
        ogObjPointer.setMaxHeight(befHeight);
        
        ogObjPointer.getParent().layout();
    }
}
